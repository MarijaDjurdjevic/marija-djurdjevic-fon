<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>  

<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>  
   
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Student - Data</title>

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" 	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

</head>
<body>

	<jsp:include page="/WEB-INF/pages/index.jsp"></jsp:include>
	
		<table>
			<tbody>
				<tr>
					<th>Exam ID</th>
					<th>Date</th>
					<th>Subject</th>
					<th>Professor</th>
				</tr>
				
				
					<tr>
						<td>${exam.examId}</td>
						<td>${exam.date}</td>
						<td>${exam.subjectDto.id}</td>
						<td>${exam.professorDto.id}</td>
					</tr>
				
			</tbody>
		</table>

</body>
</html>