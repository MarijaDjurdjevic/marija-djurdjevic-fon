<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Edit : exam</title>

<style type="text/css">
	.error{
		color:red;
	}
</style>

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" 	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>


</head>
<body>
	
	<jsp:include page="/WEB-INF/pages/index.jsp"></jsp:include>
	<p/>

	<form:form action="/<%=request.getContextPath()%>/edit" method="post" modelAttribute="examDto">
		Exam ID:<form:input type="text" path="examId" id="examId"/>
		<br/>
		<form:errors path="examId" cssClass="error"/>
		<p/>
		Date:<form:input type="date" path="date" />
		<br/>
		<form:errors path="date" cssClass="error"/>
		<p/>
		Subject:<form:input type="text" path="subject" id="subjectId"/>
		<br/>
		<form:errors path="subject" cssClass="error"/>
		<p/>
		Professor:<form:input type="text" path="profesor" id="profesorId"/>
		<br/>
		<form:errors path="profesor" cssClass="error"/>	
		
				
		<button id="save">Save</button> 
	</form:form>
	
	
</body>
</html>