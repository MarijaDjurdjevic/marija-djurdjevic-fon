<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   

<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%> 
   
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Exam - Data</title>

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" 	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

</head>
<body>

	<jsp:include page="/WEB-INF/pages/index.jsp"></jsp:include>
	
		<table>
			<tbody>
				<tr>
					<th>Exam ID</th>
					<th>Date</th>
					<th>Subject</th>
					<th>Professor</th>
										
					<th>Save</th>
					<th>Edit</th>
					<th>Delete</th>
					
					<th>Add</th>
					<th>All</th>
					<th>Exam</th>
				</tr>
				
				<c:forEach items="${exams}" var="exam">

					<c:url value="/exam/save" var="save">
						<c:param name="id" value="${exam.id}"></c:param>
					</c:url>

					<c:url value="/exam/update" var="update">
						<c:param name="id" value="${exam.id}"></c:param>
					</c:url>
					
					<c:url value="/exam/delete" var="delete">
						<c:param name="id" value="${exam.id}"></c:param>
					</c:url>
					
					<c:url value="/exam/all" var="all">
						<c:param name="id" value="${exam.id}"></c:param>
					</c:url>
					<c:url value="/exam/add" var="add">
						<c:param name="id" value="${exam.id}"></c:param>
					</c:url>
					<c:url value="/exam/exam" var="exam">
						<c:param name="id" value="${exam.id}"></c:param>
					</c:url>


					<tr>
						<td>${exam.examId}</td>
						<td>${exam.date}</td>
						<td>${exam.subjectDto.id}</td>
						<td>${exam.professorDto.id}</td>
												
						<td><a href="${save}">Save</a></td>
						<td><a href="${update}">Edit</a></td>
						<td><a href="${delete}">Delete</a></td>
						<td><a href="${add}">Add</a></td>
						<td><a href="${all}">All</a></td>
						<td><a href="${exam}">Exam</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>

</body>
</html>