<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Index page</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" 	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

</head>
</head>
<body>

	<div>
		<c:url value="/city/home" var="cityHome"></c:url>
		<a href="<c:out value="${cityHome}"/>">City : homepage</a>
	</div>

	<div>
		<c:url value="/student/home" var="studentHome"></c:url>
		<a href="<c:out value="${studentHome}"/>">Student : homepage</a>
	</div>

	<div>
		<c:url value="/professor/home" var="professorHome"></c:url>
		<a href="<c:out value="${professorHome}"/>">Professor : homepage</a>
	</div>
	<div>
		<c:url value="/subject/home" var="subjectHome"></c:url>
		<a href="<c:out value="${subjectHome}"/>">Subject: homepage</a>
	</div>
	
	<div>
		<c:url value="/exam/home" var="examHome"></c:url>
		<a href="<c:out value="${examHome}"/>">Exam : home</a>
	</div>
	
	<div>
		<c:url value="/studentExam/home" var="studentExamHome"></c:url>
		<a href="<c:out value="${studentExamHome}"/>">Student exams : input page</a>
	</div>
	
	<div>
		<c:url value="/title/home" var="titleHome"></c:url>
		<a href="<c:out value="${titleHome}"/>">Title : homepage</a>
	</div>
	
	<script type="text/javascript" src="webjars/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript"
	src="webjars/bootstrap/4.4.1/js/bootstrap.min.js"></script>

</body>
</html>