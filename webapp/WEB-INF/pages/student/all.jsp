<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>    

<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
   
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Student - Data</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" 	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

</head>
<body>

	<jsp:include page="/WEB-INF/pages/index.jsp"></jsp:include>
	<form:form action="/<%=request.getContextPath()%>/student/all" method="get" modelAttribute="studentDto">
		<table>
			<tbody>
				<tr>
					<th>Student ID</th>
					<th>Index number</th>
					<th>Firstname</th>
					<th>Lastname</th>
					<th>Email</th>
					<th>Address</th>
					<th>City</th>
					<th>Phone</th>
					<th>Current year of study</th>
					
					<th>Save</th>
					<th>Update</th>
					<th>Delete</th>
					<th>Add</th>
					<th>All</th>
					<th>Edit</th>
				</tr>
				
				<c:forEach items="${students}" var="student">

					<c:url value="/student/save" var="save">
						<c:param name="id" value="${student.id}"></c:param>
					</c:url>

					<c:url value="/student/update" var="update">
						<c:param name="id" value="${student.id}"></c:param>
					</c:url>
					
					<c:url value="/student/delete" var="delete">
						<c:param name="id" value="${student.id}"></c:param>
					</c:url>
					
					<c:url value="/student/add" var="add">
						<c:param name="id" value="${student.id}"></c:param>
					</c:url>
					
					<c:url value="/student/all" var="all">
						<c:param name="id" value="${student.id}"></c:param>
					</c:url>
					
					<c:url value="/student/edit" var="edit">
						<c:param name="id" value="${student.id}"></c:param>
					</c:url>


					<tr>
						<td>${student.id}</td>
						<td>${student.indexNumber}</td>
						<td>${student.firstname}</td>
						<td>${student.lastname}</td>
						<td>${student.email}</td>
						<td>${student.address}</td>
						<td>${student.city.id}</td>
						<td>${student.phone}</td>
						<td>${student.currentYearOfStudy}</td>
						
						<td><a href="${save}">Save</a></td>
						<td><a href="${update}">Update</a></td>
						<td><a href="${delete}">Delete</a></td>
						<td><a href="${add}">Add</a></td>
						<td><a href="${all}">All</a></td>
						<td><a href="${edit}">Edit</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		</form:form>

</body>
</html>