<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>    
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
   
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Student - Data</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" 	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

</head>
<body>
<jsp:include page="/WEB-INF/pages/index.jsp"></jsp:include>
	<p/>

	
	<form:form action="/<%=request.getContextPath()%>/all" method="get" modelAttribute="studentDto">
		<table>
			<tbody>
				<tr>
					<th>Student ID</th>
					<th>Index number</th>
					<th>Firstname</th>
					<th>Lastname</th>
					<th>Email</th>
					<th>Address</th>
					<th>City</th>
					<th>Phone</th>
					<th>Current year of study</th>
					
				</tr>
				
					<tr>
						<td>${studentDto.id}</td>
						<td>${studentDto.indexNumber}</td>
						<td>${studentDto.firstName}</td>
						<td>${studentDto.lastName}</td>
						<td>${studentDto.email}</td>
						<td>${studentDto.address}</td>
						<td>${studentDto.cityDto.id}</td>
						<td>${studentDto.phone}</td>
						<td>${studentDto.currentYearOfStudy}</td>
						
					</tr>
			
			</tbody>
		</table>
</form:form>

</body>
</html>