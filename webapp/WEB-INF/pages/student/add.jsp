<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add : student</title>

<style type="text/css">
	.error{
		color:red;
	}
</style>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" 	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>


</head>
<body>

	<jsp:include page="/WEB-INF/pages/index.jsp"></jsp:include>
	<p/>
	
	<form:form action="/<%=request.getContextPath()%>/student/save" method="post" modelAttribute="studentDto">
		<p>
		Student ID:<form:input type="text" path="id" id="id"/>
		<br/>
		<form:errors path="id" cssClass="error"/>
		<p/>
		Index number:<form:input type="text" path="indexNumber" id="indexNumberId"/>
		<br/>
		<form:errors path="indexNumber" cssClass="error"/>
		<p/>
		Firstname:<form:input type="text" path="firstname" id="firstnameId"/>
		<br/>
		<form:errors path="firstname" cssClass="error"/>
		<p/>
		Lastname:<form:input type="text" path="lastname" id="lastnameId"/>
		<br/>
		<form:errors path="lastname" cssClass="error"/>
		<p/>
		E-mail:<form:input type="text" path="email" id="emailId"/>
		<br/>
		<form:errors path="email" cssClass="error"/>
		<p/>
		Address:<form:input type="text" path="address" id="addressId"/>
		<br/>
		<form:errors path="address" cssClass="error"/>
		<p/>
		City:<form:input type="text" path="city.id" id="id"/>
		<br/>
		<form:errors path="city.id" cssClass="error"/>
		<p/>
		Phone:<form:input type="text" path="phone" id="phoneId"/>
		<br/>
		<form:errors path="phone" cssClass="error"/>
		<p/>
		Current year of study:<form:input type="text" path="currentYearOfStudy" id="currentYearOfStudyId"/>
		<br/>
		<form:errors path="currentYearOfStudy" cssClass="error"/>
		<p/>
		
		<button id="save">Save</button> 
	</form:form>
	
	
</body>
</html>