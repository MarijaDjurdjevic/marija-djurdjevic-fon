<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add : subject</title>

<style type="text/css">
	.error{
		color:red;
	}
</style>

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" 	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>


</head>
<body>
	
	<jsp:include page="/WEB-INF/pages/index.jsp"></jsp:include>
	
	<form:form action="/<%=request.getContextPath()%>subject/save" method="post" modelAttribute="subjectDto">
		Subject ID:<form:input type="text" path="id" id="id"/>
		<p>
		
		<form:errors path="id" cssClass="error"/>
		<p/>
		Name:<form:input type="text" path="name" id="nameId"/>
		<br/>
		<form:errors path="name" cssClass="error"/>
		<p/>
		Description:<form:input type="text" path="description" id="descriptionId"/>
		<br/>
		<form:errors path="description" cssClass="error"/>
		<p/>
		Year of study:<form:input type="text" path="yearOfStudy" id="yearOfStudyId"/>
		<br/>
		<form:errors path="yearOfStudy" cssClass="error"/>
		<p/>
		Semester:<form:input type="text" path="semester" id="semesterId"/>
		<br/>
		<form:errors path="semester" cssClass="error"/>
		<p/>
		
				
		<button id="save">Save</button> 
	</form:form>
	
	
</body>
</html>