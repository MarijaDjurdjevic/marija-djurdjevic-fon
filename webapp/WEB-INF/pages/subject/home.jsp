<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
     <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
     
        <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
        <%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Home : subject</title>

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" 	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

</head>
<body>

	<jsp:include page="/WEB-INF/pages/index.jsp"></jsp:include>
	<p/>

	<form:form action="/<%=request.getContextPath()%>subject/home" method="get" modelAttribute="subjectDto">
	
	
	<div>
		<c:url value="/subject/add" var="add"></c:url>
		<a href="<c:out value="${add}"/>">Add subject</a>
	</div>
	
	<div>
		<c:url value="/subject/all" var="all"></c:url>
		<a href="<c:out value="${all}"/>">All subjects</a>
	</div>
	
	<div>
		<c:url value="/subject/subject" var="subject"></c:url>
		<a href="<c:out value="${subject}"/>">Subject</a>
	</div>
	
	
	<div>
		<c:url value="/subject/update" var="update"></c:url>
		<a href="<c:out value="${update}"/>">Edit</a>
	</div>
	
	<div>
		<c:url value="/subject/delete" var="delete"></c:url>
		<a href="<c:out value="${delete}"/>">Delete</a>
	</div>
	
	</form:form>
	
</body>
</html>