<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>    
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
   
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>StudentExam - Data</title>

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" 	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

</head>
<body>

	<jsp:include page="/WEB-INF/pages/index.jsp"></jsp:include>
	
		<table>
			<tbody>
				<tr>
					<th>Exam ID</th>
					<th>Student ID</th>
					<th>Exam</th>
					<th>Date</th>
					
					
					<th>Save</th>
					<th>Edit</th>
					<th>Delete</th>
					<th>All</th>
					<th>Add</th>
				</tr>
				
				<c:forEach items="${studentExams}" var="studentExams">

					<c:url value="/studentExam/save" var="save">
						<c:param name="id" value="${studentExam.id}"></c:param>
					</c:url>

					<c:url value="/studentExam/update" var="update">
						<c:param name="id" value="${studentExam.id}"></c:param>
					</c:url>
					
					<c:url value="/studentExam/delete" var="delete">
						<c:param name="id" value="${studentExam.id}"></c:param>
					</c:url>
					
					<c:url value="/studentExam/all" var="all">
						<c:param name="id" value="${studentExam.id}"></c:param>
					</c:url>
					
					<c:url value="/studentExam/add" var="add">
						<c:param name="id" value="${studentExam.id}"></c:param>
					</c:url>


					<tr>
						<td>${studentExam.id}</td>
						<td>${studentExam.studentDto.id}</td>
						<td>${studentExam.name}</td>
						<td>${studentExam.date}</td>
												
						<td><a href="${save}">Save</a></td>
						<td><a href="${update}">Edit</a></td>
						<td><a href="${delete}">Delete</a></td>
						<td><a href="${all}">All</a></td>
						<td><a href="${add}">Add</a></td>
						
					</tr>
				</c:forEach>
			</tbody>
		</table>

</body>
</html>