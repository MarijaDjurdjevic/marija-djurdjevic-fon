<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Edit : studentExam</title>

<style type="text/css">
	.error{
		color:red;
	}
</style>

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" 	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>


</head>
<body>
<jsp:include page="/WEB-INF/pages/index.jsp"></jsp:include>
	<p/>

	
	<form:form action="/<%=request.getContextPath()%>studentExam/update" method="post" modelAttribute="studentExamDto">
		Student ID:<form:input type="text" path="id" id="id"/>
		<br/>
		<form:errors path="id" cssClass="error"/>
		<p/>
		Exam:<form:input type="text" path="exam" id="examId"/>
		<br/>
		<form:errors path="exam" cssClass="error"/>
		<p/>
		Date:<form:input type="date" path="date" id="dateId"/>
		<br/>
		<form:errors path="date" cssClass="error"/>
		<p/>
		
		
		<button id="save">Save</button> 
	</form:form>
	
	
</body>
</html>