<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>    

<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>All : city</title>

<style type="text/css">
	.error{
		color:red;
	}
</style>

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" 	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

</head>
<body>

	<jsp:include page="/WEB-INF/pages/index.jsp"></jsp:include>
	
		<table>
			<tbody>
				<tr>
					<th>City ID</th>
					<th>Number</th>
					<th>Name</th>
															
					<th>Save</th>
					<th>Edit</th>
					<th>Delete</th>
					<th>Add</th>
					<th>All</th>
					<th>Edit</th>
				</tr>
				
				<c:forEach items="${cities}" var="city">

					<c:url value="/city/save" var="save">
						<c:param name="id" value="${city.id}"></c:param>
					</c:url>

					<c:url value="/city/update" var="update">
						<c:param name="id" value="${city.id}"></c:param>
					</c:url>
					
					<c:url value="/city/delete" var="delete">
						<c:param name="id" value="${city.id}"></c:param>
					</c:url>
					
					<c:url value="/city/add" var="add">
						<c:param name="id" value="${city.id}"></c:param>
					</c:url>
					<c:url value="/city/all" var="all">
						<c:param name="id" value="${city.id}"></c:param>
					</c:url>
					<c:url value="/city/edit" var="edit">
						<c:param name="id" value="${city.id}"></c:param>
					</c:url>


					<tr>
						<td>${city.id}</td>
						<td>${city.number}</td>
						<td>${city.name}</td>
												
						<td><a href="${save}">Save</a></td>
						<td><a href="${update}">Edit</a></td>
						<td><a href="${delete}">Delete</a></td>
						<td><a href="${add}">Add</a></td>
						<td><a href="${all}">All</a></td>
						<td><a href="${edit}">Edit</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	
	
	
	
	
</body>
</html>