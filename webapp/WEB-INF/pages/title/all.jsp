<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%> 
   
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Title - Data</title>

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" 	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

</head>
<body>

	<jsp:include page="/WEB-INF/pages/index.jsp"></jsp:include>
	
		<table>
			<tbody>
				<tr>
					<th>Title ID</th>
					<th>Name</th>
										
					<th>Save</th>
					<th>Edit</th>
					<th>Delete</th>
					
					<th>Add</th>
					<th>All</th>
					<th>Title</th>
				</tr>
				
				<c:forEach items="${titles}" var="title">

					<c:url value="/title/save" var="save">
						<c:param name="id" value="${title.id}"></c:param>
					</c:url>

					<c:url value="/title/update" var="update">
						<c:param name="id" value="${title.id}"></c:param>
					</c:url>
					
					<c:url value="/title/delete" var="delete">
						<c:param name="id" value="${title.id}"></c:param>
					</c:url>
					
					<c:url value="/title/all" var="all">
						<c:param name="id" value="${title.id}"></c:param>
					</c:url>
					<c:url value="/title/add" var="add">
						<c:param name="id" value="${title.id}"></c:param>
					</c:url>
					<c:url value="/title/exam" var="exam">
						<c:param name="id" value="${title.id}"></c:param>
					</c:url>


					<tr>
						<td>${title.id}</td>
						<td>${title.name}</td>
												
						<td><a href="${save}">Save</a></td>
						<td><a href="${update}">Edit</a></td>
						<td><a href="${delete}">Delete</a></td>
						<td><a href="${add}">Add</a></td>
						<td><a href="${all}">All</a></td>
						<td><a href="${title}">Title</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>

</body>
</html>