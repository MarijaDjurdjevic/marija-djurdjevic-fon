<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>

<security:authorize access="isAuthenticated()"> Authenticated as:
	<security:authentication property="principal.username" />
</security:authorize>
	
<div>
	<c:url value="/authentication/logout" var="logout"></c:url>
	<a href="<c:out value="${logout}"/>">Logout</a>
</div>

<div>
	<c:url value="/city/add" var="cityAdd"></c:url>
	<a href="<c:out value="${cityAdd}"/>">Add city</a>
</div>

<div>
	<c:url value="/student/add" var="studentAdd"></c:url>
	<a href="<c:out value="${studentAdd}"/>">Add student</a>
</div>

<div>
	<c:url value="/professor/add" var="professorAdd"></c:url>
	<a href="<c:out value="${professorAdd}"/>">Add professor</a>
</div>

<div>
	<c:url value="/exam/add" var="examAdd"></c:url>
	<a href="<c:out value="${examAdd}"/>">Add exam</a>
</div>

<div>
	<c:url value="/studentExam/add" var="studentExamAdd"></c:url>
	<a href="<c:out value="${studentExamAdd}"/>">Add exam to student</a>
</div>

<div>
	<c:url value="/subject/add" var="subjectAdd"></c:url>
	<a href="<c:out value="${subjectAdd}"/>">Add subject</a>
</div>

<div>
	<c:url value="/title/add" var="titleAdd"></c:url>
	<a href="<c:out value="${titleAdd}"/>">Add title</a>
</div>