package marija.djurdjevic.fon.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import marija.djurdjevic.fon.entity.Student;
import marija.djurdjevic.fon.model.StudentDto;

@Component
public class StudentConverter {
	
	
	
	public StudentDto entityToDto(Student student) {
		return new StudentDto(student.getStudentId(),
				student.getIndexNumber(),
				student.getFirstname(),
				student.getLastname(),
				student.getEmail(),
				student.getAddress(),
				student.getCity(),
				student.getPhone(),
				student.getCurrentYearOfStudy());
	}
	
	public Student dtoToEntity(StudentDto studentDto) {
		return new Student(studentDto.getId(),
				studentDto.getIndexNumber(),
				studentDto.getFirstname(),
				studentDto.getLastname(),
				studentDto.getEmail(),
				studentDto.getAddress(),
				studentDto.getCity(),
				studentDto.getPhone(),
				studentDto.getCurrentYearOfStudy());
	}
	
	public List<StudentDto> entityListTODtoList(List<Student> studentList){
		List<StudentDto> studentsDto = new ArrayList<StudentDto>();
		for (Student student : studentList) {
			studentsDto.add(entityToDto(student));
		}
		return studentsDto;
	}
	
	public List<Student> dtoListTOEntityList(List<StudentDto> studentDtoList){
		List<Student> students = new ArrayList<Student>();
		for (StudentDto studentDto : studentDtoList) {
			students.add(dtoToEntity(studentDto));
		}
		return students;
	}

}
