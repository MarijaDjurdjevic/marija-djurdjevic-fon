package marija.djurdjevic.fon.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import marija.djurdjevic.fon.entity.Professor;
import marija.djurdjevic.fon.model.ProfessorDto;

@Component
public class ProfessorConverter {
	
	
	
	public ProfessorDto entityToDto(Professor professor) {
		return new ProfessorDto(professor.getProfessorId(),
				professor.getFirstname(),
				professor.getLastname(),
				professor.getEmail(),
				professor.getAddress(),
				professor.getPhone(),
				professor.getReelectionDate(),
				professor.getTitle(),
				professor.getCity());
	}
	
	public Professor dtoToEntity(ProfessorDto professorDto) {
		return new Professor(professorDto.getId(),
				professorDto.getFirstname(),
				professorDto.getLastname(),
				professorDto.getEmail(),
				professorDto.getAddress(),
				professorDto.getPhone(),
				professorDto.getReelectionDate(),
				professorDto.getTitle(),
				professorDto.getCity());
	}
	
	public List<ProfessorDto> entityListTODtoList(List<Professor> professorList){
		List<ProfessorDto> professorsDto = new ArrayList<ProfessorDto>();
		for (Professor professor : professorList) {
			professorsDto.add(entityToDto(professor));
		}
		return professorsDto;
	}
	
	public List<Professor> dtoListTOEntityList(List<ProfessorDto> professorDtoList){
		List<Professor> professors = new ArrayList<Professor>();
		for (ProfessorDto professorDto : professorDtoList) {
			professors.add(dtoToEntity(professorDto));
		}
		return professors;
	}

}
