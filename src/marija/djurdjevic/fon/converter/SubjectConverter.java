package marija.djurdjevic.fon.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import marija.djurdjevic.fon.entity.Subject;
import marija.djurdjevic.fon.model.SubjectDto;

@Component
public class SubjectConverter {

	public SubjectDto entityToDto(Subject subject) {
		return new SubjectDto(subject.getId(), subject.getName(), subject.getDescription(), subject.getYearOfStudy(), subject.getSemester());
	}
	
	public Subject dtoToEntity(SubjectDto subjectDto) {
		return new Subject(subjectDto.getId(), subjectDto.getName(), subjectDto.getDescription(), subjectDto.getYearOfStudy(), subjectDto.getSemester());
	}
	
	public List<Subject> listDtoTOlistEntity(List<SubjectDto> subjectDtoList){
		List<Subject> subjects = new ArrayList<Subject>();
		for(SubjectDto subjectDto : subjectDtoList) {
			subjects.add(dtoToEntity(subjectDto));
		}
		return subjects;
	}
	
	public List<SubjectDto> listEntityTOlistDto(List<Subject> subjectList){
		List<SubjectDto> subjectsDto = new ArrayList<SubjectDto>();
		for (Subject subject : subjectList) {
			subjectsDto.add(entityToDto(subject));
		}
		return subjectsDto;
	}
}
