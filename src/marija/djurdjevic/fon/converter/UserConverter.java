package marija.djurdjevic.fon.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import marija.djurdjevic.fon.entity.User;
import marija.djurdjevic.fon.model.UserDto;

@Component
public class UserConverter {
	
	public UserDto entityToDto(User user) {
		return new UserDto(user.getUserId(),user.getUsername(),
				user.getPassword());
				
	}
	
	public User dtoToEntity(UserDto userDto) {
		return new User(userDto.getUserId(),userDto.getUsername(), userDto.getPassword());
	}
	
	public UserDto convertEntityListToDtoList(List<User> users) {
		List<UserDto> usersDtoList = new ArrayList<UserDto>();
		for (User user : users) {
			usersDtoList.add(entityToDto(user));
		}
		return (UserDto) usersDtoList;
	}

	public List<User> convertDtoListToEntityList(List<UserDto> usersDto) {
		List<User> userEntityList = new ArrayList<User>();
		for (UserDto userDto : usersDto) {
			userEntityList.add(dtoToEntity(userDto));
		}
		return userEntityList;
	}


}
