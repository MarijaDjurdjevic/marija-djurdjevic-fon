package marija.djurdjevic.fon.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import marija.djurdjevic.fon.entity.Exam;
import marija.djurdjevic.fon.model.ExamDto;

@Component
public class ExamConverter {

	public ExamDto entityToDto(Exam exam) {
		return new ExamDto(exam.getExamId(), exam.getExamDate(), exam.getSubject(), exam.getProfessor());
	}
	
	public Exam dtoToEntity(ExamDto examDto) {
		return new Exam(examDto.getExamId(), examDto.getExamDate(), examDto.getSubject(), examDto.getProfessor());
	}
	
	public List<Exam> listDtoTOlistEntity(List<ExamDto> examDtoList){
		List<Exam> exams = new ArrayList<Exam>();
		for(ExamDto examDto : examDtoList) {
			exams.add(dtoToEntity(examDto));
		}
		return exams;
	}
	
	public List<ExamDto> listEntityTOlistDto(List<Exam> examList){
		List<ExamDto> examsDto = new ArrayList<ExamDto>();
		for (Exam exam : examList) {
			examsDto.add(entityToDto(exam));
		}
		return examsDto;
	}
}
