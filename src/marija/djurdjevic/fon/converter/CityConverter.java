package marija.djurdjevic.fon.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import marija.djurdjevic.fon.entity.City;
import marija.djurdjevic.fon.model.CityDto;

@Component
public class CityConverter {
	
	public CityDto entityToDto(City city) {
		return new CityDto(city.getId(), city.getNumber(), city.getName());
	}
	
	public City dtoToEntity(CityDto cityDto) {
		return new City(cityDto.getId(), cityDto.getNumber(), cityDto.getName());
	}
	
	public List<City> listDtoTOlistEntity(List<CityDto> cityDtoList){
		List<City> cities = new ArrayList<City>();
		for(CityDto cityDto : cityDtoList) {
			cities.add(dtoToEntity(cityDto));
		}
		return cities;
	}
	
	public List<CityDto> listEntityTOlistDto
	(List<City> cityList){
		List<CityDto> citiesDto = new ArrayList<CityDto>();
		for (City city : cityList) {
			citiesDto.add(entityToDto(city));
		}
		return citiesDto;
	}
}
