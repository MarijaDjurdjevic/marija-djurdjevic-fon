package marija.djurdjevic.fon.security;

import java.io.Serializable;
import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import marija.djurdjevic.fon.model.UserDto;

public class MyDatabaseUserDetails implements UserDetails, Serializable {
	private static final long serialVersionUID = 1L;
	private final UserDto userDto;

	public MyDatabaseUserDetails(UserDto userDto) {
		this.userDto =userDto;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return null;
	}

	@Override
	public String getPassword() {
		return userDto.getPassword();
	}

	@Override
	public String getUsername() {
		return userDto.getUsername();
	}

	@Override
	public boolean isAccountNonExpired() {
		return false;
	}

	@Override
	public boolean isAccountNonLocked() {
		return false;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return false;
	}

	@Override
	public boolean isEnabled() {
		return false;
	}

	public UserDto getUserDto() {
		return userDto;
	}

	
}
