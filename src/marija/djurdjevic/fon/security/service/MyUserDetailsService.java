package marija.djurdjevic.fon.security.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public interface MyUserDetailsService extends UserDetailsService{

	UserDetails loadUserByUsernameAndPassword(String username, String password) throws UsernameNotFoundException;

	UserDetails loadUserByUsername(String username, String password) throws UsernameNotFoundException;

}
