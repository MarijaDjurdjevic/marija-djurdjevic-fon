package marija.djurdjevic.fon.security.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import marija.djurdjevic.fon.converter.UserConverter;
import marija.djurdjevic.fon.entity.User;
import marija.djurdjevic.fon.model.UserDto;
import marija.djurdjevic.fon.repository.UserRepository;
import marija.djurdjevic.fon.security.MyDatabaseUserDetails;
import marija.djurdjevic.fon.security.service.MyUserDetailsService;

@Service
public class MyDatabaseUserDetailsService  implements MyUserDetailsService{
	@Autowired
	UserRepository userRepository;
	@Autowired 
	UserConverter userConverter;
	
	@Override
	public UserDetails loadUserByUsernameAndPassword(String username, String password) throws UsernameNotFoundException {
		User user = (User)userRepository.findUserByUsernameAndPassword(username,password);
		if (user!=null) {
			UserDto userDto = userConverter.entityToDto(user);
			return new MyDatabaseUserDetails(userDto);
		}
		throw new UsernameNotFoundException("User does not exist!");
	}

	
	


	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}





	@Override
	public UserDetails loadUserByUsername(String username, String password) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}
}
