package marija.djurdjevic.fon.formatter;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import marija.djurdjevic.fon.model.ExamDto;
import marija.djurdjevic.fon.service.ExamService;
@Component
public class ExamDtoFormatter implements Formatter<ExamDto>{
	
	
	private final ExamService examService;
	
	@Autowired
	public ExamDtoFormatter(ExamService examService) {
		System.out.println("============  ExamDtoFormatter: constructor  ============");
		this.examService = examService;
	}

	@Override
	public String print(ExamDto examDto, Locale locale) {
		System.out.println("============= ExamDtoFormatter: print ==================");
		System.out.println(examDto);
		return examDto.toString();
		
	}

	@Override
	public ExamDto parse(String text, Locale locale) throws ParseException {
		System.out.println("==============  ExamDtoFormatter: parse  ===============");
		System.out.println(text);
		
		Long number=Long.parseLong(text);
		System.out.println(number);
		
		System.out.println("=======================================================");
		ExamDto examDto = examService.findByExamId(number);
		System.out.println("=======================================================");
		return examDto;
		
	}

	public ExamService getExamService() {
		return examService;
	}

}
