package marija.djurdjevic.fon.formatter;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import marija.djurdjevic.fon.model.SubjectDto;
import marija.djurdjevic.fon.service.SubjectService;

@Component
public class SubjectDtoFormatter implements Formatter<SubjectDto>{
	
	
	private final SubjectService subjectService;
	
	@Autowired
	public SubjectDtoFormatter(SubjectService subjectService) {
		System.out.println("============  SubjectDtoFormatter: constructor  ============");
		this.subjectService = subjectService;
	}

	@Override
	public String print(SubjectDto subjectDto, Locale locale) {
		System.out.println("============= SubjectDtoFormatter: print ==================");
		System.out.println(subjectDto);
		return subjectDto.toString();
		
	}

	@Override
	public SubjectDto parse(String text, Locale locale) throws ParseException {
		System.out.println("==============  SubjectDtoFormatter: parse  ===============");
		System.out.println(text);
		
		Long number=Long.parseLong(text);
		System.out.println(number);
		
		System.out.println("=======================================================");
		SubjectDto subjectDto = subjectService.findById(number);
		System.out.println("=======================================================");
		return subjectDto;
		
	}

	public SubjectService getSubjectService() {
		return subjectService;
	}

}
