package marija.djurdjevic.fon.formatter;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import marija.djurdjevic.fon.model.StudentDto;
import marija.djurdjevic.fon.service.StudentService;

@Component
public class StudentDtoFormatter implements Formatter<StudentDto>{
	
	
	private final StudentService studentService;
	
	@Autowired
	public StudentDtoFormatter(StudentService studentService) {
		System.out.println("============  StudentDtoFormatter: constructor  ============");
		this.studentService = studentService;
	}

	@Override
	public String print(StudentDto studentDto, Locale locale) {
		System.out.println("============= ExamDtoFormatter: print ==================");
		System.out.println(studentDto);
		return studentDto.toString();
		
	}

	@Override
	public StudentDto parse(String text, Locale locale) throws ParseException {
		System.out.println("==============  ExamDtoFormatter: parse  ===============");
		System.out.println(text);
		
		Long number=Long.parseLong(text);
		System.out.println(number);
		
		System.out.println("=======================================================");
		StudentDto studentDto = studentService.findById(number);
		System.out.println("=======================================================");
		return studentDto;
		
	}

	public StudentService getStudentService() {
		return studentService;
	}

}
