package marija.djurdjevic.fon.formatter;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import marija.djurdjevic.fon.model.ProfessorDto;
import marija.djurdjevic.fon.service.ProfessorService;
@Component
public class ProfessorDtoFormatter implements Formatter<ProfessorDto>{
	
	
	private final ProfessorService professorService;
	
	@Autowired
	public ProfessorDtoFormatter(ProfessorService professorService) {
		System.out.println("============  ProfessorDtoFormatter: constructor  ============");
		this.professorService = professorService;
	}

	@Override
	public String print(ProfessorDto professorDto, Locale locale) {
		System.out.println("============= ProfessorDtoFormatter: print ==================");
		System.out.println(professorDto);
		return professorDto.toString();
		
	}

	@Override
	public ProfessorDto parse(String text, Locale locale) throws ParseException {
		System.out.println("==============  ProfessorDtoFormatter: parse  ===============");
		System.out.println(text);
		
		Long number=Long.parseLong(text);
		System.out.println(number);
		
		System.out.println("=======================================================");
		ProfessorDto professorDto = professorService.findByProfessorId(number);
		System.out.println("=======================================================");
		return professorDto;
		
	}

	public ProfessorService getProfessorService() {
		return professorService;
	}

}
