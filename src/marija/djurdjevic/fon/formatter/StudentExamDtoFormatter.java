package marija.djurdjevic.fon.formatter;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import marija.djurdjevic.fon.model.StudentExamDto;
import marija.djurdjevic.fon.service.StudentExamService;

@Component
public class StudentExamDtoFormatter implements Formatter<StudentExamDto>{
	
	
		private final StudentExamService studentExamService;
		
		@Autowired
		public StudentExamDtoFormatter(StudentExamService studentExamService) {
			System.out.println("============  StudentExamDtoFormatter: constructor  ============");
			this.studentExamService = studentExamService;
		}

		@Override
		public String print(StudentExamDto studentExamDto, Locale locale) {
			System.out.println("============= StudentExamDtoFormatter: print ==================");
			System.out.println(studentExamDto);
			return studentExamDto.toString();
			
		}

		@Override
		public StudentExamDto parse(String text, Locale locale) throws ParseException {
			System.out.println("==============  StudentExamDtoFormatter: parse  ===============");
			System.out.println(text);
			
			Long number=Long.parseLong(text);
			System.out.println(number);
			
			System.out.println("=======================================================");
			StudentExamDto studentExamDto = studentExamService.findById(number);
			System.out.println("=======================================================");
			return studentExamDto;
			
		}

		public StudentExamService getStudentExamService() {
			return studentExamService;
		}

	}
