package marija.djurdjevic.fon.formatter;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import marija.djurdjevic.fon.model.CityDto;
import marija.djurdjevic.fon.service.CityService;
@Component
public class CityDtoFormatter implements Formatter<CityDto>{
	
	
	private final CityService cityService;
	
	@Autowired
	public CityDtoFormatter(CityService cityService) {
		System.out.println("============  CityDtoFormatter: constructor  ============");
		this.cityService = cityService;
	}

	@Override
	public String print(CityDto cityDto, Locale locale) {
		System.out.println("============= CityDtoFormatter: print ==================");
		System.out.println(cityDto);
		return cityDto.toString();
		
	}

	@Override
	public CityDto parse(String text, Locale locale) throws ParseException {
		System.out.println("==============  CityDtoFormatter: parse  ===============");
		System.out.println(text);
		
		Long number=Long.parseLong(text);
		System.out.println(number);
		
		System.out.println("=======================================================");
		CityDto cityDto = cityService.findById(number);
		System.out.println("=======================================================");
		return cityDto;
		
	}

	public CityService getCityService() {
		return cityService;
	}

}
