package marija.djurdjevic.fon.formatter;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import marija.djurdjevic.fon.model.TitleDto;
import marija.djurdjevic.fon.service.TitleService;

@Component
public class TitleDtoFormatter implements Formatter<TitleDto>{
	
	
	private final TitleService titleService;
	
	@Autowired
	public TitleDtoFormatter(TitleService titleService) {
		System.out.println("============  TitleDtoFormatter: constructor  ============");
		this.titleService = titleService;
	}

	@Override
	public String print(TitleDto titleDto, Locale locale) {
		System.out.println("============= TitleDtoFormatter: print ==================");
		System.out.println(titleDto);
		return titleDto.toString();
		
	}

	@Override
	public TitleDto parse(String text, Locale locale) throws ParseException {
		System.out.println("==============  TitleDtoFormatter: parse  ===============");
		System.out.println(text);
		
		Long number=Long.parseLong(text);
		System.out.println(number);
		
		System.out.println("=======================================================");
		TitleDto titleDto = titleService.findByTitleId(number);
		System.out.println("=======================================================");
		return titleDto;
		
	}

	public TitleService getTitleService() {
		return titleService;
	}

}
