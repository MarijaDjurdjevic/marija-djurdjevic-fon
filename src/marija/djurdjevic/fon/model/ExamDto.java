package marija.djurdjevic.fon.model;

import java.io.Serializable;
import java.util.Date;

import marija.djurdjevic.fon.entity.Professor;
import marija.djurdjevic.fon.entity.Subject;

public class ExamDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long examId;
    private Date examDate;
    private Subject subject;
    private Professor professor;

    public ExamDto(Long examId, Date examDate, Subject subject, Professor professor) {
        this.examId = examId;
        this.examDate = examDate;
        this.subject = subject;
        this.professor = professor;
    }

    public ExamDto() {
    }

	public Long getExamId() {
		return examId;
	}

	public void setExamId(Long examId) {
		this.examId = examId;
	}

	public Date getExamDate() {
		return examDate;
	}

	public void setExamDate(Date examDate) {
		this.examDate = examDate;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public Professor getProfessor() {
		return professor;
	}

	public void setProfessor(Professor professor) {
		this.professor = professor;
	}

	@Override
	public String toString() {
		return "ExamDto [examId=" + examId + ", examDate=" + examDate + ", subject=" + subject + ", professor="
				+ professor + "]";
	}

    

}
