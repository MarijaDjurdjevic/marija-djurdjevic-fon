package marija.djurdjevic.fon.model;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;

public class TitleDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@NotEmpty(message = "Minimal number of characters is 1")
	private Long id;
	@NotEmpty(message = "Minimal number of characters is 3")
	private String name;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public TitleDto(@NotEmpty(message = "Minimal number of characters is 1") Long id,
			@NotEmpty(message = "Minimal number of characters is 3") String name) {
		this.id = id;
		this.name = name;
	}
	public TitleDto() {
		
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TitleDto other = (TitleDto) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "TitleDto [id=" + id + ", name=" + name + "]";
	}
	
	

}
