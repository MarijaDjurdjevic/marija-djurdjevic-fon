package marija.djurdjevic.fon.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;

import marija.djurdjevic.fon.entity.Exam;
import marija.djurdjevic.fon.entity.Student;

public class StudentExamDto implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id;
	@NotEmpty(message = "Minimal number of characters is 10")
	private Student student;
	
	private Exam exam;
	
	@Temporal(TemporalType.DATE)
	private Date date;

	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Exam getExam() {
		return exam;
	}

	public void setExam(Exam exam) {
		this.exam = exam;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	

	public StudentExamDto() {
		
	}

	public StudentExamDto(Long id, @NotEmpty(message = "Minimal number of characters is 10") Student student, Exam exam,
			Date date) {
		super();
		this.id = id;
		this.student = student;
		this.exam = exam;
		this.date = date;
	}

	@Override
	public String toString() {
		return "StudentExamDto [id=" + id + ", student=" + student + ", exam=" + exam + ", date=" + date + "]";
	}

	
	
	
	
	
	
	

}
