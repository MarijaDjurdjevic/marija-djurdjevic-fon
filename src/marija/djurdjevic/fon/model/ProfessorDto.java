package marija.djurdjevic.fon.model;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.UniqueElements;
import org.springframework.data.annotation.Reference;

import marija.djurdjevic.fon.entity.City;
import marija.djurdjevic.fon.entity.Title;

public class ProfessorDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@NotEmpty
	private Long id;
	@NotEmpty(message = "Minimal number of characters is 3")
	private String firstname;
	@NotEmpty(message = "Minimal number of characters is 3")
	private String lastname;
	@UniqueElements(message = "Must have @ character!")
	private String email;
	@NotEmpty(message = "Minimal number of characters is 3")
	private String address;
	@NotEmpty(message = "Minimal number of characters is 6")
	private String phone;
	private Date reelectionDate;
	@Reference(to = Title.class)
	private Title title;
	@Reference(to = City.class)
	private City city;
	
	public ProfessorDto() {
		
	}

	public ProfessorDto(@NotEmpty Long id,
			@NotEmpty(message = "Minimal number of characters is 3") String firstname,
			@NotEmpty(message = "Minimal number of characters is 3") String lastname,
			@UniqueElements(message = "Must have @ character!") String email,
			@NotEmpty(message = "Minimal number of characters is 3") String address,
			@NotEmpty(message = "Minimal number of characters is 6") String phone, Date reelectionDate, Title title,
			City city) {
		
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.address = address;
		this.phone = phone;
		this.reelectionDate = reelectionDate;
		this.title = title;
		this.city = city;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Date getReelectionDate() {
		return reelectionDate;
	}

	public void setReelectionDate(Date reelectionDate) {
		this.reelectionDate = reelectionDate;
	}

	public Title getTitle() {
		return title;
	}

	public void setTitle(Title title) {
		this.title = title;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	@Override
	public String toString() {
		return "ProfessorDto [id=" + id + ", firstname=" + firstname + ", lastname=" + lastname
				+ ", email=" + email + ", address=" + address + ", phone=" + phone + ", reelectionDate="
				+ reelectionDate + ", title=" + title + ", city=" + city + "]";
	}
	
	
	
	
	
	

}
