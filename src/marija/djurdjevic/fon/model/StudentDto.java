package marija.djurdjevic.fon.model;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import marija.djurdjevic.fon.entity.City;

public class StudentDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	
	private Long id;
	@NotEmpty(message = "Minimal number of characters is 10")
	private String indexNumber;
	@NotEmpty(message = "Minimal number of characters is 3")
	private String firstname;
	@NotEmpty(message = "Minimal number of characters is 3")
	private String lastname;
	@NotEmpty(message = "Must have @ character")
	private String email;
	@NotEmpty(message = "Minimal number of characters is 3")
	private String address;
	@NotNull(message = "Morate uneti ptt broj")
	private City city;
	@NotEmpty(message = "Minimal number of characters is 6")
	private String phone;
	private Long currentYearOfStudy;
	
	
	
	public String getIndexNumber() {
		return indexNumber;
	}
	public void setIndexNumber(String indexNumber) {
		this.indexNumber = indexNumber;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public City getCity() {
		return city;
	}
	public void setCity(City city) {
		this.city = city;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public Long getCurrentYearOfStudy() {
		return currentYearOfStudy;
	}
	public void setCurrentYearOfStudy(Long currentYearOfStudy) {
		this.currentYearOfStudy = currentYearOfStudy;
	}
	public StudentDto() {
		
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public StudentDto(Long id, @NotEmpty(message = "Minimal number of characters is 10") String indexNumber,
			@NotEmpty(message = "Minimal number of characters is 3") String firstname,
			@NotEmpty(message = "Minimal number of characters is 3") String lastname,
			@NotEmpty(message = "Must have @ character") String email,
			@NotEmpty(message = "Minimal number of characters is 3") String address,
			@NotNull(message = "Morate uneti ptt broj") City city,
			@NotEmpty(message = "Minimal number of characters is 6") String phone, Long currentYearOfStudy) {
		this.id = id;
		this.indexNumber = indexNumber;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.address = address;
		this.city = city;
		this.phone = phone;
		this.currentYearOfStudy = currentYearOfStudy;
	}
	@Override
	public String toString() {
		return "StudentDto [id=" + id + ", indexNumber=" + indexNumber + ", firstname=" + firstname
				+ ", lastname=" + lastname + ", email=" + email + ", address=" + address + ", city=" + city + ", phone="
				+ phone + ", currentYearOfStudy=" + currentYearOfStudy + "]";
	}
	
	
	
	
	
	

}
