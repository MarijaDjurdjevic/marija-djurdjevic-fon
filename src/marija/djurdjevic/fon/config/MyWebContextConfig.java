package marija.djurdjevic.fon.config;
import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.tiles3.TilesConfigurer;
import org.springframework.web.servlet.view.tiles3.TilesViewResolver;

import marija.djurdjevic.fon.formatter.CityDtoFormatter;
import marija.djurdjevic.fon.formatter.ExamDtoFormatter;
import marija.djurdjevic.fon.formatter.ProfessorDtoFormatter;
import marija.djurdjevic.fon.formatter.StudentDtoFormatter;
import marija.djurdjevic.fon.formatter.StudentExamDtoFormatter;
import marija.djurdjevic.fon.formatter.SubjectDtoFormatter;
import marija.djurdjevic.fon.formatter.TitleDtoFormatter;
import marija.djurdjevic.fon.service.CityService;
import marija.djurdjevic.fon.service.ExamService;
import marija.djurdjevic.fon.service.ProfessorService;
import marija.djurdjevic.fon.service.StudentExamService;
import marija.djurdjevic.fon.service.StudentService;
import marija.djurdjevic.fon.service.SubjectService;
import marija.djurdjevic.fon.service.TitleService;

//konfigurisanje bean-ova koje koristi dispatcher servlet
//za obradu zahteva
@EnableWebMvc //automatsko definisanje MapperHandler-a koji na osnovu URL-a poziva odgovarajuci kontroller
@ComponentScan(basePackages = {
		"marija.djurdjevic.fon.controller",
		"marija.djurdjevic.fon.formatter"
})
@Configuration
public class MyWebContextConfig implements WebMvcConfigurer{
	
	private CityService cityService;
	private ExamService examService;
	private ProfessorService professorService;
	private StudentService studentService;
	private StudentExamService studentExamService;
	private SubjectService subjectService;
	private TitleService titleService;
	
	
	@Autowired
	public MyWebContextConfig(CityService cityService, ExamService examService, ProfessorService professorService, 
			StudentService studentService, StudentExamService studentExamService, 
			SubjectService subjectService, TitleService titleService) {
		System.out.println("=================================================================");
		System.out.println("==================== MyWebAppContextConfig ======================");
		System.out.println("=================================================================");
		this.cityService = cityService;
		this.examService = examService;
		this.professorService = professorService;
		this.studentService = studentService;
		this.studentExamService = studentExamService;
		this.subjectService = subjectService;
		this.titleService = titleService;
		
	}
	
	
	//view resolveri
	@Bean
	public ViewResolver viewResolver() {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setPrefix("/WEB-INF/pages/");
		viewResolver.setSuffix(".jsp");
		viewResolver.setOrder(1);
		return viewResolver;
	}
	
	//Definisanje TilesViewResolver
	@Bean
	public ViewResolver tilesViewResolver() {
		TilesViewResolver tilesViewResolver = new TilesViewResolver();
		tilesViewResolver.setOrder(0);
		return tilesViewResolver;
	}
	
	//Konfigurisanje TilesViewResolvera
	@Bean
	public TilesConfigurer tilesCongigurer() {
		TilesConfigurer tilesConfigurer = new TilesConfigurer();
		tilesConfigurer.setDefinitions(
				new String[] {"/WEB-INF/views/tiles/tiles.xml"}
		);
		return tilesConfigurer;
	}
	
	@Bean
	public SimpleDateFormat simpleDateFormat() {
		return new SimpleDateFormat("yyyy-mm-dd");
	}
	
	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/").setViewName("redirect:/authentication/login");
		
	}
	
	
	@Override
	public void addFormatters(FormatterRegistry registry) {
		registry.addFormatter(new CityDtoFormatter(cityService));
		registry.addFormatter(new ExamDtoFormatter(examService));
		registry.addFormatter(new ProfessorDtoFormatter(professorService));
		registry.addFormatter(new StudentDtoFormatter(studentService));
		registry.addFormatter(new StudentExamDtoFormatter(studentExamService));
		registry.addFormatter(new SubjectDtoFormatter(subjectService));
		registry.addFormatter(new TitleDtoFormatter(titleService));
		
	}
}
