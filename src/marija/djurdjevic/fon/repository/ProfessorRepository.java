package marija.djurdjevic.fon.repository;

import org.springframework.data.jpa.repository.JpaRepository;


import org.springframework.stereotype.Repository;

import marija.djurdjevic.fon.entity.Professor;
@Repository
public interface ProfessorRepository extends JpaRepository<Professor, Long> {

	Professor findByProfessorId(Long id);
	
	

}
