package marija.djurdjevic.fon.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import marija.djurdjevic.fon.entity.StudentExam;

@Repository
public interface StudentExamRepository extends JpaRepository<StudentExam, Long> {

	

}
