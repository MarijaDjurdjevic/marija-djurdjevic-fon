package marija.djurdjevic.fon.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import marija.djurdjevic.fon.entity.Subject;

@Repository
public interface SubjectRepository extends JpaRepository<Subject, Long> {
	
	
}
