package marija.djurdjevic.fon.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import marija.djurdjevic.fon.entity.City;

@Repository
public interface CityRepository extends JpaRepository<City, Long> {
	
	

}
