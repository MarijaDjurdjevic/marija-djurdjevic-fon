package marija.djurdjevic.fon.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import marija.djurdjevic.fon.entity.Exam;

@Repository
public interface ExamRepository extends JpaRepository<Exam, Long>  {

		
	
}
