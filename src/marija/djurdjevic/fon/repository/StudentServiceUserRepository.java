package marija.djurdjevic.fon.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

import marija.djurdjevic.fon.entity.User;
@Repository
public interface StudentServiceUserRepository extends JpaRepository<User, Long> {

	User findByUsername(String username);
	
	

}
