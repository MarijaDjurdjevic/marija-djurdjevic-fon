package marija.djurdjevic.fon.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import marija.djurdjevic.fon.entity.User;


public interface UserRepository extends JpaRepository<User, Long>{

	public List<User> findUserByUsernameAndPassword(String username, String password);

	

}
