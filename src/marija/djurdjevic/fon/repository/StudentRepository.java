package marija.djurdjevic.fon.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import marija.djurdjevic.fon.entity.Student;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long>{
	

	
}
