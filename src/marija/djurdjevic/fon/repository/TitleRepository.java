package marija.djurdjevic.fon.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import marija.djurdjevic.fon.entity.Title;

@Repository
public interface TitleRepository extends JpaRepository<Title, Long> {
	

}
