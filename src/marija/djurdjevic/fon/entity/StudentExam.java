package marija.djurdjevic.fon.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "student_exam")
public class StudentExam implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    private Long id;

    @JoinColumn(name = "studentId")
    private Student student;

    @JoinColumn(name = "examId")
    private Exam exam;

    @Temporal(TemporalType.DATE)
    private Date date;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Exam getExam() {
        return exam;
    }

    public void setExam(Exam exam) {
        this.exam = exam;
    }

    public StudentExam() {

    }

    public Student getIndexNumber() {
        return student;
    }

    public void setIndexNumber(Student indexNumber) {
        this.student = indexNumber;
    }

    public StudentExam(Student indexNumber, Exam exam, Date date) {
        this.student = indexNumber;
        this.exam = exam;
        this.date = date;
    }

    @Override
    public String toString() {
        return "StudentExam [indexNumber=" + student + ", exam=" + exam + ", date=" + date + "]";
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

}
