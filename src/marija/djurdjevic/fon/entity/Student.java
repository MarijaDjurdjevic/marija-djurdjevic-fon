package marija.djurdjevic.fon.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "student")
public class Student implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String indexNumber;

    private String firstname;
    private String lastname;
    private String email;
    private String address;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "cityId")
    private City city;

    private String phone;
    private Long currentYearOfStudy;

    public Student() {
    }
    

    public String getIndexNumber() {
        return indexNumber;
    }

    public Student(Long studentId, String indexNumber, String firstname, String lastname, String email, String address, City city, String phone, Long currentYearOfStudy) {
        this.id = studentId;
        this.indexNumber = indexNumber;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.address = address;
        this.city = city;
        this.phone = phone;
        this.currentYearOfStudy = currentYearOfStudy;
    }

    public void setIndexNumber(String indexNumber) {
        this.indexNumber = indexNumber;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Long getCurrentYearOfStury() {
        return currentYearOfStudy;
    }

    public void setCurrentYearOfStury(Long currentYearOfStury) {
        this.currentYearOfStudy = currentYearOfStury;
    }

    public Long getStudentId() {
        return id;
    }

    public void setStudentId(Long studentId) {
        this.id = studentId;
    }

    public Long getCurrentYearOfStudy() {
        return currentYearOfStudy;
    }

    public void setCurrentYearOfStudy(Long currentYearOfStudy) {
        this.currentYearOfStudy = currentYearOfStudy;
    }

    @Override
    public String toString() {
        return "Student [studentId=" + id + ", indexNumber=" + indexNumber + ", firstname=" + firstname
                + ", lastname=" + lastname + ", email=" + email + ", address=" + address + ", city=" + city + ", phone="
                + phone + ", currentYearOfStudy=" + currentYearOfStudy + "]";
    }

    
   

}
