package marija.djurdjevic.fon.entity;

public enum Semester {
	
	SUMMER("Summer"), WINTER("Winter");
	
	private String type;

	private Semester(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	

}
