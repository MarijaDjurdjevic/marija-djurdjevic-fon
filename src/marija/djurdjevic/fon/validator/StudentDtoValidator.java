package marija.djurdjevic.fon.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import marija.djurdjevic.fon.model.StudentDto;

public class StudentDtoValidator implements Validator{
	

	@Override
	public boolean supports(Class<?> clazz) {
		return StudentDto.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		StudentDto studentDto = (StudentDto) target;
			
		
		if(studentDto.getIndexNumber()!=null) {
			if(studentDto.getIndexNumber().length()<10) {
				errors.rejectValue("indexNumber", "StudentDto.indexNumber", "Minimal number of characters is 10!");
			}
		}
		
		if(studentDto.getFirstname()!=null) {
			if(studentDto.getFirstname().length()<3) {
				errors.rejectValue("firstname", "StudentDto.firstname", "Minimal number of characters is 3!");
			}
		}
		
		if(studentDto.getLastname()!=null) {
			if(studentDto.getLastname().length()<3) {
				errors.rejectValue("lastname", "StudentDto.lastname", "Minimal number of characters is 3!");
			}
		}
		
		if(studentDto.getEmail()!=null) {
			if(!studentDto.getEmail().contains("@")) {
				errors.rejectValue("email", "StudentDto.email", "Must have @ character!");
			}
		}
		
		if(studentDto.getAddress()!=null) {
			if(studentDto.getAddress().length()<3) {
				errors.rejectValue("address", "StudentDto.address", "Minimal number of characters is 3!");
			}
		}
		
		if(studentDto.getPhone()!=null) {
			if(studentDto.getPhone().length()<6) {
				errors.rejectValue("phone", "StudentDto.phone", "Minimal number of characters is 6!");
			}
		}
			
			
		}
		
	

}
