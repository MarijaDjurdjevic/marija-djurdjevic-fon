package marija.djurdjevic.fon.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import marija.djurdjevic.fon.model.SubjectDto;

public class SubjectDtoValidator implements Validator{

	@Override
	public boolean supports(Class<?> clazz) {
		return SubjectDto.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		SubjectDto subjectDto = (SubjectDto) target;
		

		if(subjectDto.getName()!=null) {
			if(subjectDto.getName().length()<3) {
				errors.rejectValue("name", "SubjectDto.name", "Minimal number of characters is 3!");
			}
		}
		
	}

}
