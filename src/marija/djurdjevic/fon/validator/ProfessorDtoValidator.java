package marija.djurdjevic.fon.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import marija.djurdjevic.fon.model.ProfessorDto;

public class ProfessorDtoValidator implements Validator{
	

	@Override
	public boolean supports(Class<?> clazz) {
		return ProfessorDto.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		
		ProfessorDto professorDto = (ProfessorDto) target;
		
		if(professorDto.getFirstname()!=null) {
			if(professorDto.getFirstname().length()<3) {
				errors.rejectValue("firstname", "ProfessorDto.firstname", "Minimal number of characters is 3!");
			}
		}
		
		if(professorDto.getLastname()!=null) {
			if(professorDto.getLastname().length()<3) {
				errors.rejectValue("lastname", "ProfessorDto.lastname", "Minimal number of characters is 3!");
			}
		}
		
		if(professorDto.getEmail()!=null) {
			if(!professorDto.getEmail().contains("@")) {
				errors.rejectValue("email", "ProfessorDto.email", "Must have @ character!");
			}
		}
		
		if(professorDto.getAddress()!=null) {
			if(professorDto.getAddress().length()<3) {
				errors.rejectValue("address", "ProfessorDto.address", "Minimal number of characters is 3!");
			}
		}
		
		if(professorDto.getPhone()!=null) {
			if(professorDto.getPhone().length()<6) {
				errors.rejectValue("phone", "ProfessorDto.phone", "Minimal number of characters is 6!");
			}
		}
			
				
	}

}
