package marija.djurdjevic.fon.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import marija.djurdjevic.fon.converter.ExamConverter;
import marija.djurdjevic.fon.converter.ProfessorConverter;
import marija.djurdjevic.fon.converter.SubjectConverter;
import marija.djurdjevic.fon.entity.Exam;
import marija.djurdjevic.fon.model.ExamDto;
import marija.djurdjevic.fon.repository.ExamRepository;
import marija.djurdjevic.fon.service.ExamService;

@Service
@Transactional
public class ExamServiceImpl implements ExamService{
	
	private final ExamRepository examRepository;
	
	private final ExamConverter examConverter;
	
	private final SubjectConverter subjectConverter;
	
	private final ProfessorConverter professorConverter;
	
	@Autowired
	public ExamServiceImpl(ExamRepository examRepository, ExamConverter examConverter,
			SubjectConverter subjectConverter, ProfessorConverter professorConverter) {
		super();
		this.examRepository = examRepository;
		this.examConverter = examConverter;
		this.subjectConverter = subjectConverter;
		this.professorConverter = professorConverter;
	}

	@Override
	public void save(ExamDto examDto) {
		examRepository.save(examConverter.dtoToEntity(examDto));
	}
	
	@Override
	public void update(Long id, ExamDto examDto) {
		Exam exam = examRepository.findById(id).get();
		exam.setExamId(examDto.getExamId());
		exam.setExamDate(examDto.getExamDate());
		exam.setSubject(examDto.getSubject());
		exam.setProfessor(examDto.getProfessor());
		examRepository.save(exam);
		
	}

	@Override
	public List<ExamDto> getAll() {
		List<ExamDto> examDtoList = new ArrayList<ExamDto>();
		List<Exam> exams = examRepository.findAll();
		for (Exam exam : exams) {
			examDtoList.add(examConverter.entityToDto(exam));
		}
		return examDtoList;
	}

	@Override
	public ExamDto findByExamId(Long examId) {
		return examConverter.entityToDto(examRepository.findById(examId).get());
	}

	@Override
	public void delete(Long id) {
		examRepository.deleteById(id);
	}

	public SubjectConverter getSubjectConverter() {
		return subjectConverter;
	}

	public ProfessorConverter getProfessorConverter() {
		return professorConverter;
	}

}
