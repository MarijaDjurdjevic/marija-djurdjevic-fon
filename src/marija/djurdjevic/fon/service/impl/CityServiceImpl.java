package marija.djurdjevic.fon.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import marija.djurdjevic.fon.converter.CityConverter;
import marija.djurdjevic.fon.entity.City;
import marija.djurdjevic.fon.model.CityDto;
import marija.djurdjevic.fon.repository.CityRepository;
import marija.djurdjevic.fon.service.CityService;

@Service
@Transactional
public class CityServiceImpl implements CityService {
	
	private  CityRepository cityRepository;
	private  CityConverter cityConverter;
	
	@Autowired
	public CityServiceImpl(CityRepository cityRepository, CityConverter cityConverter) {
		super();
		this.cityRepository = cityRepository;
		this.cityConverter = cityConverter;
	}
	
	@Override
	public void save(CityDto cityDto) {
		cityRepository.save(cityConverter.dtoToEntity(cityDto));
	}

	

	@Override
	public List<CityDto> getAll() {
		
		List<City> cities = cityRepository.findAll();
		
		List<CityDto> citiesDto = cityConverter.listEntityTOlistDto(cities);
		
		return citiesDto;
	}

	@Override
	public CityDto findById(Long id) {
		Optional<City> city = cityRepository.findById(id);
		if(city.isPresent()) {
			return cityConverter.entityToDto(city.get());
		}else {
			return null;
		}
		
	}

	@Override
	public void delete(Long id) {
		cityRepository.deleteById(id);
		
	}

	

	
	
	
	
}