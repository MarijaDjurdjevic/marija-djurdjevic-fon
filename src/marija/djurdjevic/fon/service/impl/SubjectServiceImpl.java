package marija.djurdjevic.fon.service.impl;

import java.util.ArrayList;
import java.util.List;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import marija.djurdjevic.fon.converter.SubjectConverter;
import marija.djurdjevic.fon.entity.Subject;
import marija.djurdjevic.fon.model.SubjectDto;
import marija.djurdjevic.fon.repository.SubjectRepository;
import marija.djurdjevic.fon.service.SubjectService;


@Service
@Transactional
public class SubjectServiceImpl implements SubjectService{
	
	private final SubjectRepository subjectRepository;
	
	private final SubjectConverter subjectConverter;
	
	@Autowired
	public SubjectServiceImpl(SubjectRepository subjectRepository, SubjectConverter subjectConverter) {
		super();
		this.subjectRepository = subjectRepository;
		this.subjectConverter = subjectConverter;
		
	}

	@Override
	public void save(SubjectDto subjectDto) {
		subjectRepository.save(subjectConverter.dtoToEntity(subjectDto));
	}
	
	@Override
	public void update(Long id, SubjectDto subjectDto) {
		Subject subject = (Subject) subjectRepository.findById(id).get();
		subject.setId(subjectDto.getId());
		subject.setName(subjectDto.getName());
		subject.setDescription(subjectDto.getDescription());
		subject.setYearOfStudy(subjectDto.getYearOfStudy());
		subject.setSemester(subjectDto.getSemester());
		
		subjectRepository.save(subject);
	}

	@Override
	public List<SubjectDto> getAll() {
		List<SubjectDto> subjectDtoList = new ArrayList<SubjectDto>();
		List<Subject> subjects = subjectRepository.findAll();
		for (Subject subject : subjects) {
			subjectDtoList.add(subjectConverter.entityToDto(subject));
		}
		return subjectDtoList;
	}

	@Override
	public SubjectDto findById(Long subjectId) {
		return subjectConverter.entityToDto(subjectRepository.findById(subjectId).get());
	}

	@Override
	public void delete(Long id) {
		subjectRepository.deleteById(id);
	}

}
