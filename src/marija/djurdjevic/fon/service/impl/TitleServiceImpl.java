package marija.djurdjevic.fon.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import marija.djurdjevic.fon.entity.Title;
import marija.djurdjevic.fon.model.TitleDto;
import marija.djurdjevic.fon.repository.TitleRepository;
import marija.djurdjevic.fon.service.TitleService;

@Service
@Transactional
public class TitleServiceImpl implements TitleService{
	
	
	private final TitleRepository titleRepository;
	
	@Autowired
	public TitleServiceImpl(TitleRepository titleRepository) {
		super();
		this.titleRepository = titleRepository;
	}

	@Override
	public void save(TitleDto titleDto) {
		Title title = new Title(titleDto.getId(), titleDto.getName());
		titleRepository.save(title);
		
	}

	@Override
	public List<TitleDto> getAll() {
		List<TitleDto> titles = new ArrayList<TitleDto>();
		List<Title> titleEntities = titleRepository.findAll();
		for (Title titleEntity : titleEntities) {
			titles.add(new TitleDto(titleEntity.getId(), titleEntity.getName()));
		}
		return titles;
	}

	@Override
	public TitleDto findByTitleId(Long id) {
		Title titleEntity = titleRepository.findById(id).get();
		TitleDto titleDto = new TitleDto(titleEntity.getId(), titleEntity.getName());
		return titleDto;
	}

	@Override
	public void delete(Long id) {
		titleRepository.deleteById(id);
	}

}
