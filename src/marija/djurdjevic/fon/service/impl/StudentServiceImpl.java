package marija.djurdjevic.fon.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import marija.djurdjevic.fon.converter.CityConverter;
import marija.djurdjevic.fon.converter.StudentConverter;
import marija.djurdjevic.fon.entity.Student;
import marija.djurdjevic.fon.model.StudentDto;
import marija.djurdjevic.fon.repository.StudentRepository;
import marija.djurdjevic.fon.service.StudentService;


@Service
@Transactional
public class StudentServiceImpl implements StudentService{
	
	private final StudentRepository studentRepository;
	
	private final StudentConverter studentConverter;
	
	private final CityConverter cityConverter;
	
	@Autowired
	public StudentServiceImpl( StudentRepository studentRepository, StudentConverter studentConverter, CityConverter cityConverter) {
		
		this.studentRepository = studentRepository;
		this.studentConverter = studentConverter;
		this.cityConverter = cityConverter;
	}

	
	@Override
	public void save(StudentDto studentDto) {
		studentRepository.save(studentConverter.dtoToEntity(studentDto));
	}
	
	@Override
	public void update(Long id, StudentDto studentDto) {
		Student student = studentRepository.findById(id).get();
		student.setIndexNumber(studentDto.getIndexNumber());
		student.setFirstname(studentDto.getFirstname());
		student.setLastname(studentDto.getLastname());
		student.setEmail(studentDto.getEmail());
		student.setAddress(studentDto.getAddress());
		student.setCity(studentDto.getCity());
		student.setPhone(studentDto.getPhone());
		student.setCurrentYearOfStudy(studentDto.getCurrentYearOfStudy());
		studentRepository.save(student);
		
	}

	@Override
	public List<StudentDto> getAll() {
		List<StudentDto> studentDtoList = new ArrayList<StudentDto>();
		List<Student> students = studentRepository.findAll();
		for (Student student : students) {
			studentDtoList.add(studentConverter.entityToDto(student));
		}
		return studentDtoList;
	}

	@Override
	public StudentDto findById(Long id) {
		return studentConverter.entityToDto(studentRepository.findById(id).get());
	}

	@Override
	public void deleteById(Long id) {
		studentRepository.deleteById(id);
		
	}

	public CityConverter getCityConverter() {
		return cityConverter;
	}

}
