package marija.djurdjevic.fon.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import marija.djurdjevic.fon.converter.ExamConverter;
import marija.djurdjevic.fon.converter.StudentConverter;
import marija.djurdjevic.fon.entity.StudentExam;
import marija.djurdjevic.fon.model.StudentExamDto;
import marija.djurdjevic.fon.repository.StudentExamRepository;
import marija.djurdjevic.fon.service.StudentExamService;

@Service
@Transactional
public class StudentExamServiceImpl implements StudentExamService{
	
	private final StudentExamRepository studentExamRepository;
	private final StudentConverter studentConverter;
	private final ExamConverter examConverter;
	
	@Autowired
	public StudentExamServiceImpl(StudentExamRepository studentExamRepository, StudentConverter studentConverter,
			ExamConverter examConverter) {
		super();
		this.studentExamRepository = studentExamRepository;
		this.studentConverter = studentConverter;
		this.examConverter = examConverter;
	}

	public void save(StudentExamDto studentExamDto) {
		StudentExam studentExam = new StudentExam(studentExamDto.getStudent(), studentExamDto.getExam(), studentExamDto.getDate());
		studentExamRepository.save(studentExam);
	}
	
	public void update(Long id, StudentExamDto studentExamDto) {
		StudentExam studentExam = studentExamRepository.findById(id).get();
		studentExam.setStudent(studentExamDto.getStudent());
		studentExam.setExam(studentExamDto.getExam());
		studentExam.setDate(studentExamDto.getDate());
		studentExamRepository.save(studentExam);
	}
	
	public List<StudentExamDto> getAll() {
		List<StudentExamDto> studentExams = new ArrayList<StudentExamDto>();
		List<StudentExam> studentExamEntities = studentExamRepository.findAll();
		for (StudentExam studentExamEntity : studentExamEntities) {
			studentExams.add(new StudentExamDto(studentExamEntity.getId(), studentExamEntity.getIndexNumber(), studentExamEntity.getExam(), studentExamEntity.getDate()));
		}
		return studentExams;
	}
	
	public StudentExamDto findById(Long id) {
		StudentExam studentExamEntity = studentExamRepository.findById(id).get();
		StudentExamDto studentExamDto = new StudentExamDto(studentExamEntity.getId(),studentExamEntity.getIndexNumber(), studentExamEntity.getExam(),studentExamEntity.getDate());
		return studentExamDto;
	}

	@Override
	public void delete(Long id) {
		studentExamRepository.deleteById(id);
		
	}

	public StudentConverter getStudentConverter() {
		return studentConverter;
	}

	public ExamConverter getExamConverter() {
		return examConverter;
	}

	

}
