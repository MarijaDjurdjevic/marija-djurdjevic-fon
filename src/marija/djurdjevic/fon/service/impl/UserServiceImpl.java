package marija.djurdjevic.fon.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import marija.djurdjevic.fon.converter.UserConverter;
import marija.djurdjevic.fon.entity.User;
import marija.djurdjevic.fon.model.UserDto;
import marija.djurdjevic.fon.repository.UserRepository;
import marija.djurdjevic.fon.service.UserService;

@Service
public class UserServiceImpl implements UserService{

	
	private final UserRepository userRepository;
	private final UserConverter userConverter;
	
	
	@Autowired
	public UserServiceImpl(UserRepository userRepository, UserConverter userConverter) {
		this.userRepository = userRepository;
		this.userConverter = userConverter;
	}
	
	@Override
	public List<UserDto> findUserByUsernameAndPassword(UserDto userDto) throws Exception {
		List<User> users = userRepository.findUserByUsernameAndPassword(userDto.getUsername(), userDto.getPassword());
		List<UserDto> listUserDto = userConverter.convertEntityListToDtoList(users);
		if(listUserDto.isEmpty()) {
			throw new Exception("User does not exist!");
		}
		return listUserDto;
	}

}
