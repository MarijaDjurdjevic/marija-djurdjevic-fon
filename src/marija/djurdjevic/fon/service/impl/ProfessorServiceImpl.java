package marija.djurdjevic.fon.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import marija.djurdjevic.fon.converter.CityConverter;
import marija.djurdjevic.fon.converter.ProfessorConverter;
import marija.djurdjevic.fon.entity.Professor;
import marija.djurdjevic.fon.model.ProfessorDto;
import marija.djurdjevic.fon.repository.ProfessorRepository;
import marija.djurdjevic.fon.service.ProfessorService;

@Service
@Transactional
public class ProfessorServiceImpl implements ProfessorService{
	
	
	private final ProfessorRepository professorRepository;
	private final ProfessorConverter professorConverter;
	private final CityConverter cityConverter;
	
	
	@Autowired
	public ProfessorServiceImpl(ProfessorRepository professorRepository,
			ProfessorConverter professorConverter, CityConverter cityConverter) {
		this.professorRepository = professorRepository;
		this.professorConverter = professorConverter;
		this.cityConverter = cityConverter;
	}

	@Override
	public void save(ProfessorDto professorDto) {
		professorRepository.save(professorConverter.dtoToEntity(professorDto));
		
	}
	
	@Override
	public void update(Long id, ProfessorDto professorDto) {
		Professor professor = professorRepository.findByProfessorId(id);
		professor.setProfessorId(professorDto.getId());
		professor.setFirstname(professorDto.getFirstname());
		professor.setLastname(professorDto.getLastname());
		professor.setEmail(professorDto.getEmail());
		professor.setAddress(professorDto.getAddress());
		professor.setPhone(professorDto.getPhone());
		professor.setReelectionDate(professorDto.getReelectionDate());
		professor.setCity(professorDto.getCity());
		professor.setTitle(professorDto.getTitle());
		professorRepository.save(professor);
	}

	@Override
	public List<ProfessorDto> getAll() {
		List<ProfessorDto> professorDtoList = new ArrayList<ProfessorDto>();
		List<Professor> professors = professorRepository.findAll();
		for (Professor professor : professors) {
			professorDtoList.add(professorConverter.entityToDto(professor));
		}
		return professorDtoList;
	}

	@Override
	public ProfessorDto findByProfessorId(Long professorId) {
		return professorConverter.entityToDto(professorRepository.findByProfessorId(professorId));
	}

	@Override
	public void delete(Long id) {
		professorRepository.deleteById(id);
		
	}

	public CityConverter getCityConverter() {
		return cityConverter;
	}

}
