package marija.djurdjevic.fon.service;

import java.util.List;

import marija.djurdjevic.fon.model.SubjectDto;

public interface SubjectService {
	
	void save(SubjectDto subjectDto);
	
	List<SubjectDto> getAll();
	
	void delete(Long id);

	void update(Long id, SubjectDto subjectDto);

	SubjectDto findById(Long subjectId);

	

}
