package marija.djurdjevic.fon.service;

import java.util.List;

import marija.djurdjevic.fon.model.ExamDto;

public interface ExamService {
	
	void save(ExamDto examDto);
	
	List<ExamDto> getAll();
	
	ExamDto findByExamId(Long examId);

	void delete(Long id);

	void update(Long id, ExamDto examDto);

}
