package marija.djurdjevic.fon.service;

import java.util.List;

import marija.djurdjevic.fon.model.TitleDto;

public interface TitleService {
	
void save(TitleDto titleDto);
	
	List<TitleDto> getAll();
	
	TitleDto findByTitleId(Long titleId);

	void delete(Long id);

}
