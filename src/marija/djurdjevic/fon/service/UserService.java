package marija.djurdjevic.fon.service;

import java.util.List;

import marija.djurdjevic.fon.model.UserDto;

public interface UserService {
	
	public List<UserDto> findUserByUsernameAndPassword(UserDto userDto) throws Exception;
	
}


