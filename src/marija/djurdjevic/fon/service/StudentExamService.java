package marija.djurdjevic.fon.service;

import java.util.List;

import marija.djurdjevic.fon.model.StudentExamDto;

public interface StudentExamService {
	
	void save(StudentExamDto studentExamDto);
	
	List<StudentExamDto> getAll();
	
	StudentExamDto findById(Long id);

	void delete(Long id);

}
