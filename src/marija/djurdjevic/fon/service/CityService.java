package marija.djurdjevic.fon.service;

import java.util.List;

import marija.djurdjevic.fon.model.CityDto;

public interface CityService {
	
	void save(CityDto cityDto);
	List<CityDto> getAll();
	CityDto findById(Long id);
	void delete(Long id);
	

}
