package marija.djurdjevic.fon.service;

import java.util.List;

import marija.djurdjevic.fon.model.ProfessorDto;

public interface ProfessorService {
	
void save(ProfessorDto professorDto);
	
	List<ProfessorDto> getAll();
	
	ProfessorDto findByProfessorId(Long professorId);

	void delete(Long id);

	void update(Long id, ProfessorDto professorDto);

	

}
