package marija.djurdjevic.fon.service;

import java.util.List;

import marija.djurdjevic.fon.model.StudentDto;

public interface StudentService {
	
	public void save(StudentDto studentDto);
	
	public List<StudentDto> getAll();
	
	public StudentDto findById(Long id);

	

	public void update(Long id, StudentDto studentDto);

	public void deleteById(Long id);

	

}
