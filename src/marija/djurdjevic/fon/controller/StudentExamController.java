package marija.djurdjevic.fon.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import marija.djurdjevic.fon.model.ExamDto;
import marija.djurdjevic.fon.model.StudentDto;
import marija.djurdjevic.fon.model.StudentExamDto;
import marija.djurdjevic.fon.service.ExamService;
import marija.djurdjevic.fon.service.StudentExamService;
import marija.djurdjevic.fon.service.StudentService;

@Controller
@RequestMapping(value = "/studentExam")
public class StudentExamController {
	
	private StudentExamService studentExamService;
	private StudentService studentService;
	private ExamService examService;

	@Autowired
	public StudentExamController (StudentExamService studentExamService,StudentService studentService,ExamService examService) {
		this.studentExamService = studentExamService;
		this.setStudentService(studentService);
		this.setExamService(examService);
	}

	public StudentExamService getStudentExamService() {
		return studentExamService;
	}
	
	@GetMapping(value = "home")
	public ModelAndView home() {
		System.out.println("=================================================================");
		System.out.println("================StudentExamController: home() ===================");
		System.out.println("=================================================================");
		ModelAndView modelAndView = new ModelAndView("studentExam/home");
		return modelAndView;
	}
	
	@GetMapping(value = "add")
	public ModelAndView add() {
		System.out.println("===============================================================");
		System.out.println("============== StudentExamController: add() ===================");
		System.out.println("===============================================================");
		ModelAndView modelAndView = new ModelAndView("studentExam/add");
		StudentExamDto studentExamDto = new StudentExamDto();
		modelAndView.addObject("studentExamDto", studentExamDto);
		return modelAndView;
	}
	
	@PostMapping(value = "save")
	public ModelAndView save(@Valid @ModelAttribute(name = "studentExamDto") StudentExamDto studentExamDto,
			BindingResult result) {
		System.out.println("=================================================================");
		System.out.println(studentExamDto);
		System.out.println("=================================================================");

		ModelAndView modelAndView = new ModelAndView();
		if (result.hasErrors()) {
			System.out.println("========================= NOT OK ===========================");
			modelAndView.setViewName("studentExam/add");
			modelAndView.addObject("studentExamDto", studentExamDto);
		} else {
			System.out.println("===========================  OK ============================");
			modelAndView.setViewName("studentExam/all");
			studentExamService.save(studentExamDto);
		}
		return modelAndView;
	}
	
	@GetMapping(value = "edit")
	public ModelAndView edit(@RequestParam("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("studentExam/edit");
		modelAndView.addObject("studentExamDto", studentExamService.findById(id));
		return modelAndView;
	}
	
	@GetMapping(value = "delete")
	public ModelAndView remove(@RequestParam("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("studentExam/all");
		studentExamService.delete(id);
		return modelAndView;
	}
	
	@PostMapping(value = "update")
	public ModelAndView update(@Valid @ModelAttribute("studentExamDto") StudentExamDto studentExamDto, BindingResult result) {
		ModelAndView modelAndView = new ModelAndView();
		if (result.hasErrors()) {
			modelAndView.setViewName("studentExam/edit");
			modelAndView.addObject("studentExamDto", studentExamDto);
		} else {
			studentExamService.save(studentExamDto);
			modelAndView.setViewName("studentExam/all");
		}
		return modelAndView;
	}
	
	@GetMapping(value = "all")
	public ModelAndView all() {
		ModelAndView modelAndView = new ModelAndView("studentExam/all");
		return modelAndView;
	}

	
	
	@GetMapping(value = "studentExam")
	public ModelAndView details(@RequestParam("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("studentExam/studentExam");
		modelAndView.addObject("studentExamDto", studentExamService.findById(id));
		return modelAndView;
	}

	public StudentService getStudentService() {
		return studentService;
	}

	public void setStudentService(StudentService studentService) {
		this.studentService = studentService;
	}

	public ExamService getExamService() {
		return examService;
	}

	public void setExamService(ExamService examService) {
		this.examService = examService;
	}

	@ModelAttribute(name = "exams")
	public List<ExamDto> exams() {
		return examService.getAll();
	}

	@ModelAttribute(name = "students")
	public List<StudentDto> students() {
		return studentService.getAll();
	}

	@ModelAttribute(name = "studentExam")
	public StudentExamDto examRegistrationDto() {
		return new StudentExamDto();
	}

}
