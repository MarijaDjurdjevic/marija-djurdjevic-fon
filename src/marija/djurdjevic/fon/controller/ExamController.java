package marija.djurdjevic.fon.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import marija.djurdjevic.fon.model.ExamDto;
import marija.djurdjevic.fon.model.ProfessorDto;
import marija.djurdjevic.fon.model.SubjectDto;
import marija.djurdjevic.fon.service.ExamService;
import marija.djurdjevic.fon.service.ProfessorService;
import marija.djurdjevic.fon.service.SubjectService;

@Controller
@RequestMapping(value = "/exam")
public class ExamController {
	
	private final ExamService examService;
	private final SubjectService subjectService;
	private final ProfessorService professorService;
	

	@Autowired
	public ExamController (ExamService examService,SubjectService subjectService,ProfessorService professorService) {
		this.examService = examService;
		this.professorService = professorService;
		this.subjectService = subjectService;
	}

	public ExamService getExamService() {
		return examService;
	}
	
	@GetMapping(value = "home")
	public ModelAndView home() {
		System.out.println("=================================================================");
		System.out.println("====================  ExamController: home()  ===================");
		System.out.println("=================================================================");
		ModelAndView modelAndView = new ModelAndView("exam/home");
		return modelAndView;
	}
	
	@GetMapping(value = "add")
	public ModelAndView add() {
		System.out.println("==================================================================");
		System.out.println("==================== ExamController: add()     ===================");
		System.out.println("==================================================================");
		ModelAndView modelAndView = new ModelAndView("exam/add");
		ExamDto examDto = new ExamDto();
		modelAndView.addObject("examDto", examDto);
		return modelAndView;
	}
	
	@PostMapping(value = "save")
	public ModelAndView save(@Valid @ModelAttribute(name = "examDto") ExamDto examDto,
			BindingResult result) {
		System.out.println("=================================================================");
		System.out.println(examDto);
		System.out.println("=================================================================");

		ModelAndView modelAndView = new ModelAndView();
		if (result.hasErrors()) {
			System.out.println("========================= NOT OK ===========================");
			modelAndView.setViewName("exam/add");
			modelAndView.addObject("examDto", examDto);
		} else {
			System.out.println("===========================  OK ============================");
			modelAndView.setViewName("exam/all");
			examService.save(examDto);
		}
		return modelAndView;
	}
	
	@GetMapping(value = "edit")
	public ModelAndView edit(@RequestParam("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("exam/edit");
		modelAndView.addObject("examDto", examService.findByExamId(id));
		return modelAndView;
	}
	
	@GetMapping(value = "delete")
	public ModelAndView remove(@RequestParam("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("exam/all");
		examService.delete(id);
		return modelAndView;
	}
	
	@PostMapping(value = "update")
	public ModelAndView update(@Valid @ModelAttribute("examDto") ExamDto examDto, BindingResult result) {
		ModelAndView modelAndView = new ModelAndView();
		if (result.hasErrors()) {
			modelAndView.setViewName("exam/edit");
			modelAndView.addObject("examDto", examDto);
		} else {
			examService.save(examDto);
			modelAndView.setViewName("exam/all");
		}
		return modelAndView;
	}

	@GetMapping(value = "all")
	public ModelAndView all() {
		ModelAndView modelAndView = new ModelAndView("exam/all");
		return modelAndView;
	}
	
	@GetMapping(value = "exam")
	public ModelAndView details(@RequestParam("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("exam/exam");
		modelAndView.addObject("examDto", examService.findByExamId(id));
		return modelAndView;
	}

	public SubjectService getSubjectService() {
		return subjectService;
	}

	public ProfessorService getProfessorService() {
		return professorService;
	}
	
	@ModelAttribute(name = "exams")
	public List<ExamDto> exams() {
		return examService.getAll();
	}

	@ModelAttribute(name = "subjects")
	public List<SubjectDto> subjects() {
		return subjectService.getAll();
	}

	@ModelAttribute(name = "professors")
	public List<ProfessorDto> professors() {
		return professorService.getAll();
	}

}
