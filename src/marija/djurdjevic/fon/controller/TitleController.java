package marija.djurdjevic.fon.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import marija.djurdjevic.fon.model.TitleDto;
import marija.djurdjevic.fon.service.TitleService;

@Controller
@RequestMapping(value = "/title")
public class TitleController {
	
	private TitleService titleService;
	private TitleDto titleDto;

	public TitleService getTitleService() {
		return titleService;
	}
	
	@Autowired
	public TitleController(TitleService titleService) {
		this.titleService = titleService;
	}
	
	@GetMapping(value = "home")
	public ModelAndView home() {
		System.out.println("===========================================================");
		System.out.println("================TitleController: home() ===================");
		System.out.println("===========================================================");
		ModelAndView modelAndView = new ModelAndView("title/home");
		return modelAndView;
	}
	
	@GetMapping(value = "add")
	public ModelAndView add() {
		System.out.println("==========================================================");
		System.out.println("============== TitleController: add() ====================");
		System.out.println("==========================================================");
		ModelAndView modelAndView = new ModelAndView("title/add");
		titleDto = new TitleDto();
		modelAndView.addObject("titleDto", titleService);
		return modelAndView;
	}
	
	@PostMapping(value = "save")
	public ModelAndView save(@Valid @ModelAttribute(name = "titleDto") TitleDto titleDto,
			BindingResult result) {
		System.out.println("=================================================================");
		System.out.println(titleDto);
		System.out.println("=================================================================");

		ModelAndView modelAndView = new ModelAndView();
		if (result.hasErrors()) {
			System.out.println("========================= NOT OK ===========================");
			modelAndView.setViewName("title/add");
			modelAndView.addObject("titleDto", titleDto);
		} else {
			System.out.println("===========================  OK ============================");
			modelAndView.setViewName("title/all");
			titleService.save(titleDto);
		}
		return modelAndView;
	}
	
	@GetMapping(value = "edit")
	public ModelAndView edit(@RequestParam("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("title/edit");
		modelAndView.addObject("titleDto", titleService.findByTitleId(id));
		return modelAndView;
	}
	
	@GetMapping(value = "delete")
	public ModelAndView remove(@RequestParam("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("title/all");
		titleService.delete(id);
		return modelAndView;
	}
	
	@PostMapping(value = "update")
	public ModelAndView update(@Valid @ModelAttribute("stitleDto") TitleDto titleDto, BindingResult result) {
		ModelAndView modelAndView = new ModelAndView();
		if (result.hasErrors()) {
			modelAndView.setViewName("title/edit");
			modelAndView.addObject("titleDto", titleDto);
		} else {
			titleService.save(titleDto);
			modelAndView.setViewName("title/all");
		}
		return modelAndView;
	}
	
	@GetMapping(value = "all")
	public ModelAndView all() {
		ModelAndView modelAndView = new ModelAndView("title/all");
		return modelAndView;
	}

	public TitleDto getTitleDto() {
		return titleDto;
	}

	@GetMapping(value = "title")
	public ModelAndView details(@RequestParam("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("title/title");
		modelAndView.addObject("titleDto", titleService.findByTitleId(id));
		return modelAndView;
	}
	
	
	

}
