package marija.djurdjevic.fon.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import marija.djurdjevic.fon.model.CityDto;
import marija.djurdjevic.fon.model.ProfessorDto;
import marija.djurdjevic.fon.model.TitleDto;
import marija.djurdjevic.fon.service.CityService;
import marija.djurdjevic.fon.service.ProfessorService;
import marija.djurdjevic.fon.service.TitleService;
import marija.djurdjevic.fon.validator.ProfessorDtoValidator;

@Controller
@RequestMapping(value = "/professor")
public class ProfessorController {
	
private final ProfessorService professorService;
private final CityService cityService;
private final TitleService titleService;
	
ProfessorController(ProfessorService professorService,CityService cityService,TitleService titleService){
		this.professorService = professorService;
		this.cityService = cityService;
		this.titleService = titleService;
	}
	
	@GetMapping(value = "home")
	public String home() {
		System.out.println("===================================================================");
		System.out.println("==============   ProfessorController: home()    ===================");
		System.out.println("===================================================================");
		return "professor/home";
	}
	
	@GetMapping(value = "add")
	public ModelAndView add(HttpServletRequest request, HttpServletResponse response) {
		System.out.println("====================================================================");
		System.out.println("===============   ProfessorController: add()     ===================");
		System.out.println("====================================================================");
		ModelAndView modelAndView = new ModelAndView("professor/add");
		
		ProfessorDto professorDto = new ProfessorDto();
		modelAndView.addObject("professorDto",professorDto);
		
		return modelAndView;
	}
	
	@PostMapping(value = "save")
	public ModelAndView save(@ModelAttribute(name="professorDto") @Valid ProfessorDto professorDto, BindingResult result) {
	
		System.out.println("===================================================================================");
		System.out.println("===============   ProfessorController: save(@ModelAttribute)    ===================");
		System.out.println("===================================================================================");
		
		if (result.hasErrors()) {
			ModelAndView modelAndView=new ModelAndView("professor/add");
			System.out.println("====================   NOT OK   ===================");
			modelAndView.addObject("professorDto", professorDto);
			return modelAndView;
		}else {
			ModelAndView modelAndView=new ModelAndView("professor/home");
			System.out.println(professorDto);
			System.out.println("====================   OK   ===================");
			professorService.save(professorDto);
			return modelAndView;
		}
	}
	
	@GetMapping(value = "edit")
	public ModelAndView edit(@RequestParam("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("professor/edit");
		modelAndView.addObject("professorDto", professorService.findByProfessorId(id));
		return modelAndView;
	}
	
	@GetMapping(value = "delete")
	public ModelAndView remove(@RequestParam("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("professor/all");
		professorService.delete(id);
		return modelAndView;
	}
	
	@PostMapping(value = "update")
	public ModelAndView update(@Valid @ModelAttribute("examDto") ProfessorDto professorDto, BindingResult result) {
		ModelAndView modelAndView = new ModelAndView();
		if (result.hasErrors()) {
			modelAndView.setViewName("professor/edit");
			modelAndView.addObject("professorDto", professorDto);
		} else {
			professorService.save(professorDto);
			modelAndView.setViewName("professor/all");
		}
		return modelAndView;
	}
	
	@GetMapping(value = "all")
	public ModelAndView all() {
		ModelAndView modelAndView = new ModelAndView("professor/all");
		return modelAndView;
	}
	
	@GetMapping(value = "professor")
	public ModelAndView details(@RequestParam("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("professor/professor");
		modelAndView.addObject("professorDto", professorService.findByProfessorId(id));
		return modelAndView;
	}


	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(new ProfessorDtoValidator());
	}

	public TitleService getTitleService() {
		return titleService;
	}

	public CityService getCityService() {
		return cityService;
	}
	
	@ModelAttribute(value = "cities")
	public List<CityDto> cities() {
		return cityService.getAll();
	}

	@ModelAttribute(value = "professorDto")
	public ProfessorDto professorDto() {
		ProfessorDto professorDto = new ProfessorDto();
		return professorDto;
	}

	@ModelAttribute(value = "professors")
	public List<ProfessorDto> professorsDto() {
		return professorService.getAll();
	}

	@ModelAttribute(name = "titles")
	public List<TitleDto> titleDto() {
		return titleService.getAll();
	}
}
