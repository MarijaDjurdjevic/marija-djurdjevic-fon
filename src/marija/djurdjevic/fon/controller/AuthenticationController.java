package marija.djurdjevic.fon.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import marija.djurdjevic.fon.model.UserDto;


@Controller
@RequestMapping(value = "authentication")
public class AuthenticationController {
	
	@GetMapping(value = "login")
	public ModelAndView login() {
		return new ModelAndView("authentication/login");
	}
	
	@PostMapping(value = "login")
	public ModelAndView authenticate(@ModelAttribute ("userDto") UserDto userDto) {
		System.out.println("===============================================================");
		System.out.println("===========AuthenticationController: authenticate()============");
		System.out.println("===============================================================");
		System.out.println(userDto);
		
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("index");
		
				
		return modelAndView;
	}
	
	@GetMapping(value = "logout")
	public String logout(HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth!=null) {
			new SecurityContextLogoutHandler().logout(request, response,auth);
		}
		return "redirect:/authentication/login";
	}
	
	
	
	@ModelAttribute(name = "userDto")
	private UserDto generateUserDto() {
		return new UserDto();
	}
}
