package marija.djurdjevic.fon.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import marija.djurdjevic.fon.model.CityDto;
import marija.djurdjevic.fon.model.StudentDto;
import marija.djurdjevic.fon.service.CityService;
import marija.djurdjevic.fon.service.StudentService;
import marija.djurdjevic.fon.validator.StudentDtoValidator;

@Controller
@RequestMapping(value = "/student")
public class StudentController {
	
	private final StudentService studentService;
	private final CityService cityService;
	
	StudentController(StudentService studentService, CityService cityService){
		this.studentService = studentService;
		this.cityService  = cityService;
	}
	
	
	@GetMapping(value = "home")
	public String home() {
		System.out.println("===================================================================");
		System.out.println("================   StudentController: home()    ===================");
		System.out.println("===================================================================");
		return "student/home";
	}
	
	@GetMapping(value = "add")
	public ModelAndView add(HttpServletRequest request, HttpServletResponse response) {
		System.out.println("====================================================================");
		System.out.println("=================   StudentController: add()     ===================");
		System.out.println("====================================================================");
		ModelAndView modelAndView = new ModelAndView("student/add");
		
		StudentDto studentDto = new StudentDto();
		modelAndView.addObject("studentDto",studentDto);
		
		return modelAndView;
	}
	
	@PostMapping(value = "save")
	public ModelAndView save(@ModelAttribute(name="studentDto") @Valid StudentDto studentDto, BindingResult result) {
	
		System.out.println("===================================================================================");
		System.out.println("=================   StudentController: save(@ModelAttribute)    ===================");
		System.out.println("===================================================================================");
		ModelAndView modelAndView=new ModelAndView();
		if (result.hasErrors()) {
			
			System.out.println("====================   NOT OK    ===================");
			modelAndView.setViewName("student/add");
			modelAndView.addObject("studentDto", studentDto);
			return modelAndView;
		}else {
			modelAndView=new ModelAndView("student/home");
			System.out.println(studentDto);
			System.out.println("====================   OK        ===================");
			modelAndView.setViewName("student/home");
			studentService.save(studentDto);
			
		}
		return modelAndView;
	}
	
	@PostMapping(value = "edit")
	public ModelAndView edit(@RequestParam("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("student/edit");
		modelAndView.addObject("studentDto", studentService.findById(id));
		return modelAndView;
	}
	
	@GetMapping(value = "delete")
	public ModelAndView remove(@RequestParam("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("student/all");
		studentService.deleteById(id);
		return modelAndView;
	}
	
	@PostMapping(value = "update")
	public ModelAndView update(@Valid @ModelAttribute("studentDto") StudentDto studentDto, BindingResult result) {
		ModelAndView modelAndView = new ModelAndView();
		if (result.hasErrors()) {
			modelAndView.setViewName("student/edit");
			modelAndView.addObject("studentDto", studentDto);
		} else {
			studentService.save(studentDto);
			modelAndView.setViewName("student/all");
		}
		return modelAndView;
	}
	
	@GetMapping(value = "all")
	public ModelAndView all() {
		ModelAndView modelAndView = new ModelAndView("student/all");
		return modelAndView;
	}

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(new StudentDtoValidator());
	}
	 
	@GetMapping(value = "student")
	public ModelAndView details(@RequestParam("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("student/student");
		modelAndView.addObject("studentDto", studentService.findById(id));
		return modelAndView;
	}
	
	@ModelAttribute(value = "cities")
	public List<CityDto> cities() {
		return cityService.getAll();
	}

	@ModelAttribute(value = "students")
	public List<StudentDto> students() {
		return studentService.getAll();
	}


	public CityService getCityService() {
		return cityService;
	}
	
	

}
