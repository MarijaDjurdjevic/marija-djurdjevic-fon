package marija.djurdjevic.fon.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import marija.djurdjevic.fon.model.SubjectDto;
import marija.djurdjevic.fon.service.SubjectService;
import marija.djurdjevic.fon.validator.SubjectDtoValidator;

@Controller
@RequestMapping(value = "/subject")
public class SubjectController {
	
	private final SubjectService subjectService;
	@Autowired
	public SubjectController(SubjectService subjectService) {
		this.subjectService = subjectService;
	}
	@GetMapping(value = "home")
	public ModelAndView home() {
		System.out.println("====================================================================");
		System.out.println("====================SubjectController: home()  ===================");
		System.out.println("====================================================================");
		ModelAndView modelAndView = new ModelAndView("subject/home");
		return modelAndView;
	}
	@GetMapping(value = "add")
	public ModelAndView add() {
		System.out.println("====================================================================");
		System.out.println("==================== SubjectController: add()     ===================");
		System.out.println("====================================================================");
		ModelAndView modelAndView = new ModelAndView("subject/add");
		SubjectDto subjectDto = new SubjectDto();
		modelAndView.addObject("subjectDto", subjectDto);
		return modelAndView;
	}
	@PostMapping(value = "save")
	public ModelAndView save(@Valid @ModelAttribute(name = "subjectDto") SubjectDto subjectDto,
			BindingResult result) {
		System.out.println("================================  =================================");
		System.out.println(subjectDto);
		System.out.println("================================  =================================");

		ModelAndView modelAndView = new ModelAndView();
		if (result.hasErrors()) {
			System.out.println("========================= NOT OK =============================");
			modelAndView.setViewName("subject/add");
			modelAndView.addObject("subjectDto", subjectDto);
		} else {
			System.out.println("===========================  OK ==============================");
			modelAndView.setViewName("subject/all");
			subjectService.save(subjectDto);
		}
		return modelAndView;
	}

	@GetMapping(value = "edit")
	public ModelAndView edit(@RequestParam("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("subject/edit");
		modelAndView.addObject("subjectDto", subjectService.findById(id));
		return modelAndView;
	}
	
	@PostMapping(value = "update")
	public ModelAndView update(@Valid @ModelAttribute("subjectDto") SubjectDto subjectDto, BindingResult result) {
		ModelAndView modelAndView = new ModelAndView();
		if (result.hasErrors()) {
			modelAndView.setViewName("subject/edit");
			modelAndView.addObject("subjectDto", subjectDto);
		} else {
			subjectService.save(subjectDto);
			modelAndView.setViewName("subject/all");
		}
		return modelAndView;
	}
	@GetMapping(value = "delete")
	public ModelAndView remove(@RequestParam("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("subject/all");
		subjectService.delete(id);
		return modelAndView;
	}

	@GetMapping(value = "all")
	public ModelAndView all() {
		ModelAndView modelAndView = new ModelAndView("subject/all");
		return modelAndView;
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(new SubjectDtoValidator());
	}
	
	@GetMapping(value = "subject")
	public ModelAndView details(@RequestParam("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("subject/subject");
		modelAndView.addObject("subjectDto", subjectService.findById(id));
		return modelAndView;
	}

}

