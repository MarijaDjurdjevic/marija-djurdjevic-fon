package marija.djurdjevic.fon.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import marija.djurdjevic.fon.model.CityDto;
import marija.djurdjevic.fon.service.CityService;

@Controller
@RequestMapping(value = "/city")
public class CityController {
	
	private final CityService cityService;
	
	@Autowired
	public CityController(CityService cityService) {
		this.cityService = cityService;
	}
	public CityService getCityService() {
		return cityService;
	}
	
	
	@GetMapping(value = "home")
	public ModelAndView home() {
		System.out.println("==================================================================");
		System.out.println("==================   CityController: home()    ===================");
		System.out.println("==================================================================");
		ModelAndView modelAndView = new ModelAndView("city/home");
		return modelAndView;
	}
	
		
	@GetMapping(value = "add")
	public ModelAndView add() {
		System.out.println("==================================================================");
		System.out.println("==================== CityController: add()   =====================");
		System.out.println("==================================================================");
		ModelAndView modelAndView = new ModelAndView("city/add");
		CityDto cityDto = new CityDto();
		modelAndView.addObject("cityDto", cityDto);
		return modelAndView;
	}
	@PostMapping(value = "save")
	public ModelAndView save(@Valid @ModelAttribute(name = "cityDto") CityDto cityDto,
			BindingResult result) {
		System.out.println("=================================================================");
		System.out.println(cityDto);
		System.out.println("=================================================================");

		ModelAndView modelAndView = new ModelAndView();
		if (result.hasErrors()) {
			System.out.println("======================= NOT OK =============================");
			modelAndView.setViewName("city/add");
			modelAndView.addObject("cityDto", cityDto);
		} else {
			System.out.println("=========================  OK ==============================");
			modelAndView.setViewName("city/all");
			cityService.save(cityDto);
		}
		return modelAndView;
	}

	@GetMapping(value = "edit")
	public ModelAndView edit(@RequestParam("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("city/edit");
		modelAndView.addObject("cityDto", cityService.findById(id));
		return modelAndView;
	}
	
	@PostMapping(value = "update")
	public ModelAndView update(@Valid @ModelAttribute("cityDto") CityDto cityDto, BindingResult result) {
		ModelAndView modelAndView = new ModelAndView();
		if (result.hasErrors()) {
			modelAndView.setViewName("city/edit");
			modelAndView.addObject("cityDto", cityDto);
		} else {
			cityService.save(cityDto);
			modelAndView.setViewName("city/all");
		}
		return modelAndView;
	}
	
	@GetMapping(value = "delete")
	public ModelAndView remove(@RequestParam("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("city/all");
		cityService.delete(id);
		return modelAndView;
	}
	

	@GetMapping(value = "all")
	public ModelAndView all() {
		ModelAndView modelAndView = new ModelAndView("city/all");
		return modelAndView;
	}
	
	@GetMapping(value = "city")
	public ModelAndView details(@RequestParam("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("city/city");
		modelAndView.addObject("cityDto", cityService.findById(id));
		return modelAndView;
	}
	

	
}